<?php
/**
 * Copyright 2024 Paul Kraus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

/********0*********1********2********3********4********5********6********7********/
$email_message_text = "Update on {$conv_info_list['name_visible']}.\r\n\r\nThank you for being a member of Smofcon 40! 
We hope you enjoyed the convention, whether you attended the con in person in Providence or participated online. 
Because the convention was fully hybrid, recordings of all the program items were made, and these will be available to 
all Smofcon 40 members for one year through December 31, 2024! Please see information below on how to access the 
recordings. If you know someone who would benefit from the recordings, we are selling $25 post-con memberships which 
include access to them, so spread the word. Please have them go to https://smofcon40.org/registration-information/ for 
more information.\r\n
*************************\r\n*** COVID Information ***\r\n*************************\r\nAs of December 6 we have received one report of someone who attended Smofcon with Covid.\r\n
Visit https://smofcon40.org/covid-info/ for more information.\r\n
We will continue to accept reports of positive Covid tests until Monday, December 11. If you test positive, please
send us an email at covid@smofcon40.org with as much detail as you are able to about what parts of Smofcon 40 you took
part in and whether or not it is OK to publish your name along with the report on our website.\r\n 
***************************************\r\n*** Accessing Convention Recordings ***\r\n***************************************\r\nHere is your link to access the online component of {$conv_info_list['name_visible']}.\r\n\r\nYour link to participate in the convention is:\r\n{$conv_auth_link}\r\n
This is a unique link for your email address {$email_to}.\r\nIf you want to use a different email address, please email
hybrid@smofcon40.org\r\nand we will update your record.\r\n\r\nOnce you join the online portion of the convention, you
will see the program items and each program item will have a link to view that program item on YouTube. All items will
be available for re-watching through December 31, 2024 as we said above.\r\n
Smofcon 40 also has a Discord Server for the convention, and this will continue to be available for discussions about
panels,asking for help, and even just fun. The Smofcon 40 Discord Server invite is https://discord.gg/McvvC2jBSf . We
plan to keep this Discord Server available for at least one month longer than the panels will be available for viewing
(i.e. through January 31st, 2025). Please do not share this outside of Smofcon attendees or any place publicly
(including Facebook).\r\n
If you know of someone that didn't get this email and should have,\r\nplease have them check their spam folder or email hybrid@smofcon40.org\r\n\r\nSmofcon's Code of Conduct, Covid, and Mask policies can be found\r\nat https://smofcon40.org/policies/\r\n\r\nIf you have any problems, please either email us at registration@smofcon40.org or hybrid@smofcon40.org\r\n\r\n";
