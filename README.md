This is the virtualCon system originally developed by Paul Kraus for use by Albacon 2021.

The goal is to provide gated access to a convention's online resources. In this first case the online resources include
a konOpas instance that includes links to Zoom meetings/webinars and Discord server where the virtual convention will
occur.

# Change in layout / code repos coming.

I realized that in an effort to make VirtCon multi-tenant, I was combining lots of different things in one code repos
and that is bad. Over the next few weeks I hope to refactor VirtCon into HybridCon and lay out the files differently:

1. Core code and required functions
2. Per-Con code (e.g. /albacon)
3. Per-Con html (e.g. /public_html/albacon)
4. Perhaps a move to composer for all requirements instead of replying on the OS layer to provide more than core PHP
5. Perhaps a move to Postgres
6. Perhaps a move of code repos to Jet Brains Spaces (unlimited free projects)

# Requirements

- MySQL database
- Access to the konOpas data
- maybe access to the Zambia DB
- PHP 7+ execution environment

# Top Level Design

`index.php` is the master script that handles all incoming requests. It looks for a number of things and sends the user
to the right place based on what is found.

- authentication is handled vi a GUID associated with an email address. This means end users do not need to remember
  passwords, just the email they used for registering. The GUID is mapped to what the user has access to. The most basic
  access is as a convention member (attendee) where they are automatically sent the konOpas instance with participant
  links.

PHP Packages Needed (FreeBSD)

* php74-7.4.27 PHP Scripting Language
* php74-bcmath-7.4.27 The bcmath shared extension for php
* php74-composer2-2.2.2 Dependency Manager for PHP, version 2.x
* php74-ctype-7.4.27 The ctype shared extension for php
* php74-curl-7.4.27 The curl shared extension for php
* php74-dom-7.4.27 The dom shared extension for php
* php74-fileinfo-7.4.27 The fileinfo shared extension for php
* php74-filter-7.4.27 The filter shared extension for php
* php74-gd-7.4.27 The gd shared extension for php
* php74-gettext-7.4.27 The gettext shared extension for php
* php74-imap-7.4.27 The imap shared extension for php
* php74-intl-7.4.27 The intl shared extension for php
* php74-json-7.4.27 The json shared extension for php
* php74-ldap-7.4.27 The ldap shared extension for php
* php74-mbstring-7.4.27 The mbstring shared extension for php
* php74-mysqli-7.4.27 The mysqli shared extension for php
* php74-opcache-7.4.27 The opcache shared extension for php
* php74-openssl-7.4.27 The openssl shared extension for php
* php74-pecl-APCu-5.1.21 APC User Caching
* php74-pecl-uuid-1.1.0 UUID extension in PHP
* php74-phar-7.4.27 The phar shared extension for php
* php74-readline-7.4.27 The readline shared extension for php
* php74-session-7.4.27 The session shared extension for php
* php74-simplexml-7.4.27 The simplexml shared extension for php
* php74-snmp-7.4.27 The snmp shared extension for php
* php74-sockets-7.4.27 The sockets shared extension for php
* php74-xml-7.4.27 The xml shared extension for php
* php74-xmlreader-7.4.27 The xmlreader shared extension for php
* php74-xmlwriter-7.4.27 The xmlwriter shared extension for php
* php74-xsl-7.4.27 The xsl shared extension for php
* php74-zip-7.4.27 The zip shared extension for php
