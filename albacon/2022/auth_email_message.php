<?php
/**
 * Copyright 2024 Paul Kraus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

$email_message_text = "Albacon 2022 is behind us, mostly. We are reaching out to you a few more times.\r\n\r\nThis year we tried something very new for us, a true Hybrid Convention, with panelists and attendees both local (in-person) and remote (via Zoom). We are asking all of you to fill out a brief survey and tell us how we did. The survey will remain open for submissions through December 1st, 2022. Trust that we will be reading all of the responses.\r\n\r\nYou can fill out the form at this link: https://forms.office.com/r/JHujW1GyTN \r\n\r\nSubmissions shall all be anonymous.\r\n\r\nWe are also madly editing the recordings from Albacon 2022 and are planning on making them available to anyone with a valid membership for Albacon 20222, probably for 30 days. Once the recordings are ready we will reach out to you one last time with your own private link for viewing them.\r\n\r\nThank you for supporting Albacon.\r\n\r\n";

