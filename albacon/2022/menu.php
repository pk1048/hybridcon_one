<?php
/*
 * Albacon 2022 Menu
 */
echo "<p>You are: {$_SESSION['name_first']} {$_SESSION['name_last']}";
echo "<br>{$_SESSION['email']}</p>";
if ( $_SESSION['name_badge'] != "" ) {
	echo "<p>Your badge name is: {$_SESSION['name_badge']}</p>";
}
echo "<h3>Go to:</h3>";
echo "<ul>";
echo "<li><a href='index.php'>Home</a></li>";
echo "<li><a href='index.php?page=schedule'>Schedule</a></li>";
echo "<li><a href='index.php?page=memberlist'>Membership List</a></li>";
if ( $_SESSION['flag_admin'] == 1 || $_SESSION['flag_committee'] == 1 || $_SESSION['flag_staff'] == 1 || $_SESSION['flag_tech'] == 1 || $_SESSION['flag_program'] == 1 ) {
	/*
	 * Menu items that anyone who has more roles that just member
	 */

}
if ( $_SESSION['flag_admin'] == 1 ) {
	/*
	 * Menu items just for Admin role
	 */
	echo "<li><a href='index.php?page=email'>Send Email</a></li>";
}
echo "</ul>";
