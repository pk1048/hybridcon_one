<?php
/**
 * Copyright 2024 Paul Kraus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

$email_message_text = "Welcome to {$conv_info_list['name_visible']}.\r\n\r\nHere is the info you need to access the online schedule and virtual components of {$conv_info_list['name_visible']}.\r\n\r\nThe convention starts Friday evening at about 6:00 PM ET and our traditional Ice Cream Social kicks off at 8:00 PM ET just off the hotel lobby. We are hoping to have a virtual connection to the Ice Cream Social for those who are attending virtually. Follow this link: https://discord.gg/G3jjvqfD to our Discord server, click the green check box on the #Welcome channel, and then you should be able to find the virtual-con-suite a/v channel, that is where the Ice Cream Social will be virtually (if we can get it working).\r\n\r\nYour link to participate in the convention is:\r\n{$conv_auth_link}\r\n\r\nPlease save it as you may need to use it each time you return to {$conv_info_list['name_visible']}. You may use it as may times as you like.\r\n\r\nPlease note that this site is still being actively worked on, just like in-person conventions we are working right up the last minute (and maybe even after). So if something looks odd, or is broken, or changes, do not dismay.\r\n\r\nIf you have any problems, please either email us at {$conv_info_list['reg_email']} or {$conv_info_list['help_contact']} \r\n\r\n";

