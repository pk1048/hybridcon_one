<?php
/**
 * Copyright 2024 Paul Kraus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

$email_message_text = "Albacon 2023 continues today!\r\n\r\n.
Here is the info you need to access the schedule and online components of Albacon 2023.\r\n\r\n
Programming starts at 10:00 AM ET and runs until 11:00 PM tonight, starting back up tomorrow at 10:30 AM.\r\n\r\n
Please log into Discord and then follow this link: https://discord.gg/XVuTVs2Hxb to our Discord server if you need help or just wish to chat, click the green check box on the #Welcome channel, and then you should be able to find the rest of the convention.\r\n\r\n
Your link to the Hybrid Portal is:\r\n{$conv_auth_link}\r\n\r\n
The link above includes the full schedule and online items.\r\n\r\n
Please save it as you may need to use it each time you return to Albacon 2023. You may use it as may times as you like.\r\n\r\n
If you have any problems, please either email us at {$conv_info_list['reg_email']} or post in the Discord Helpdesk. \r\n\r\n";

