#!/usr/local/bin/php
<?php
/**
 * Copyright 2024 Paul Kraus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
/*
 * Script to pull program info from Zambia for use by the virtcon konOpas
 */
$zambia_path = '/home/albacon/zambia.albacon.org';
$virtcon_path = '/home/virtcon';
$albacon_2023_path = "$virtcon_path/albacon/2023";
$albacon_2023_htdocs_path = "/home/albacon/schedule.albacon.org";
// require_once("$virtcon_path/virtcon.config");
require_once("$albacon_2023_path/sched_func.php");
require_once("$virtcon_path/include/log_functions.php");
define('LOG_FILE', "/home/virtcon/logs/fetch_konOpas.log");
ini_set('display_errors', false);
ini_set('log_errors', true);
ini_set('error_log', LOG_FILE);
$konopas_file = "$albacon_2023_htdocs_path/konOpasData.jsonp";
define('MY_PID', getmypid());
$results = retrieveKonOpasData();
if ( $results["message_error"] ) {
	write_log("ERROR", MY_PID, "{$results['message_error']}");
	exit(1);
} else if ( $results["json"] ) {
	$resultsFile = fopen("{$konopas_file}", "wb");
	if ( $resultsFile === false ) {
		write_log("ERROR", MY_PID, "Can't open {$konopas_file} for writing.");
		exit(1);
	}
	if ( fwrite($resultsFile, $results["json"]) === false ) {
		write_log("ERROR", MY_PID, "Error writing to {$konopas_file}.");
		exit(1);
	}
	exit();
} else {
	write_log("ERROR", MY_PID, "retrieveKonOpasData() did not return expected result or error indicator.");
	exit(1);
}
?>
