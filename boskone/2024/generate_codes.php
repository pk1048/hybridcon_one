<?php
/**
 * Copyright 2024 Paul Kraus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

/*
 * Command line / CRON script to generate One Time Codes and populate the one_time_codes table
 */

ini_set('display_errors', true);

define('SCRIPT_PATH', dirname($argv['0']));
define('SCRIPT_NAME', substr(basename($argv['0']), 0, -4));
define('SCRIPT_PATH_PARENT', dirname($argv['0'], 3));

// per convention overrides
if ( is_file(SCRIPT_PATH . "/" . SCRIPT_NAME . ".config") ) include_once SCRIPT_PATH . "/" . SCRIPT_NAME . ".config";
if ( is_file(SCRIPT_PATH . "/virtcon.config") ) include_once SCRIPT_PATH . "/virtcon.config";

// Default configuration (required)
require_once(SCRIPT_PATH_PARENT . "/virtcon.config");

/*******************************************************************************
 * Flags for enhanced logging (VERBOSE), diagnostics (DEBUG), and test mode (TEST)
 */
if ( !defined('DEBUG') ) define('DEBUG', false);
if ( !defined('VERBOSE') ) define('VERBOSE', false);
if ( !defined('TEST') ) define('TEST', false);
/*******************************************************************************
 * Log locations and values used for logging
 */
if ( !defined('LOG_DIR') ) define('LOG_DIR', "$log_dir");
ini_set('log_errors', true);
ini_set('error_log', LOG_DIR . "/" . SCRIPT_NAME . "_php.log");
define('LOG_FILE', LOG_DIR . "/" . SCRIPT_NAME . ".log");
define('MY_PID', getmypid());
/*******************************************************************************
 * Includes
 */
$inc_path = SCRIPT_PATH_PARENT . "/include";
$inc_files_a = scandir($inc_path);
foreach ($inc_files_a as $inc_file) {
	if ( isset($suffix) ) unset($suffix);
	$suffix = substr("$inc_file", -4, 4);
	if ( "$suffix" == ".php" ) {
		if ( "$inc_file" != "session_operations.php" ) include_once "{$inc_path}/{$inc_file}";
	}
}
/*****************************************************************************/
write_log("INFO", MY_PID, SCRIPT_NAME . " starting");


$virtcon_dbx = connect_db(VIRTCON_DB_USER, VIRTCON_DB_PASS, VIRTCON_DB_NAME, VIRTCON_DB_HOST);

$count = 0;
$count_dupe = 0;
$conv_id = CONV_ID;
while ( $count < OTC_LIMIT ) {
	$seed = uniqid();
	$new_otc = hash('crc32b', $seed);
	$check_otc_sql = "SELECT * FROM one_time_codes WHERE one_time_code = '{$new_otc}'";
	$check_otc_result = mysqli_query($virtcon_dbx, "$check_otc_sql");
	$check_num_rows = mysqli_num_rows($check_otc_result);
	if ( $check_num_rows == 0 ) {
		$count++;
		$otc[] = $new_otc;
		$otc_insert_sql = "INSERT INTO one_time_codes (one_time_code, conv_id) VALUE ('$new_otc','$conv_id')";
		mysqli_query($virtcon_dbx, "$otc_insert_sql");
		$auth_url = URL . "?otc={$new_otc}";
		file_put_contents(OUTPUT_FILE, "{$auth_url},{$new_otc}\n", FILE_APPEND);
		echo "{$seed} {$new_otc} \n";
	} else {
		$count_dupe++;
	}
}
if ( isset($virtcon_dbx) ) {
	mysqli_close($virtcon_dbx);
}
echo "Codes: {$count}\n";
echo "Duplicates: {$count_dupe}\n";
