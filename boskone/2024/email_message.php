<?php
/**
 * Copyright 2024 Paul Kraus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

$email_subject = "Welcome to Boskone 61, Online Login Information";
$email_message_text = "{$member_info_list['name_badge']},\r\n\r\nWelcome to Boskone 61.\r\n\r\nBoskone 61 will be taking place at the Westin Boston Seaport District and Online, February 9-11, 2024. Programming in the Main Ballroom (Harbor 1) will be available online as well as in-person. This email will provide access for those wishing to join via zoom. It will also provide the access to view the events that are recorded after Boskone 61 is over (for about a month.)\r\n\r\nIn Person registration hours are:
    Friday   1:00 PM - 8:30 PM
    Saturday 9:00 AM - 6:00 PM
    Sunday   9:00 AM - Noon
\r\nProgram items start Friday, February 9 at 2pm EST, and end Sunday, February 11 at 4pm EST. Special events in the Harbor 1 room include
    Friday, February 9
        7:30 PM, Opening Ceremony: Meet the Guests
        8:30 PM, PMRP presents The King in Yellow.
    Saturday, February 10
        8:00 PM, Boskone 61 Awards Ceremony
        10:00 PM, Teseracte presents Dr. Horrible and the Buffy Musical.
    Sunday, February 11
        2:30 PM Convention Feedback Session\r\n
If you are attending Boskone in person, to get your badges, you will go to the Registration area on the upper level of the Westin (to the left if you enter the front door, or take the elevator to the top if you park in the garage.) Please bring your IDs and masks. \r\n\r\nYour link to participate in the online portion of the convention is:\r\n\r\n{$conv_auth_link}\r\n\r\nThis is a unique link for your email address {$member_info_list['email']}. If you want to use a different email address or have a different badge name, please email registration@boskone.org and we will update your record. If you receive multiple emails, or the name above is for a family or friend and not you, please let registration@boskone.org know that as well. (There are some records with duplicate emails because multiple memberships were purchased at the same time.)\r\n\r\nOnce you join the online portion of the convention, you will see the program items (under the Schedule link). Each one that is available online will have a link to join the Zoom Webinar for that program item. Online items will be available for re-watching for about a month after the convention.\r\n\r\nWe will also be using Discord this year. There will be an online Consuite, online Fan Tables, areas to continue discussions after the online panels, as well as other discussion areas. This is a year round server similar to some other fannish groups. In order to join our Discord server:\r\n\r\n
        1) If you are already on Discord make sure you are signed in to Discord (otherwise you will wind up with a second profile).
        2) Join via the \"NESFA and Boskone\" Discord Server via https://www.tinyurl.com/BoskoneDiscord.
        3) Acknowledge the Code of Conduct in the #welcome channel.
        4) Go to the #roles-and-ribbons channel and pick the self-service roles and ribbons (including Pronouns and also NESFA Member and / or Boskone Staff if appropriate).
        5) Go to the #special-role-needed channel and say what you are working on to get any special roles needed; Tech Staff is the most likely role needed for Tech Staff or Zoom Hosts.\r\n
You will not see the channels mentioned in 4 and 5 until you do 3.\r\n\r\nIf you know of someone that didn't get this email and should have, please have them check their spam folder or email registration@boskone.org\r\n\r\nBoskone's Code of Conduct, Covid, and Mask policies can be found at https://boskone.org/about/policies/\r\n\r\nIf you have any problems, Please email helpdesk@boskone.org";
