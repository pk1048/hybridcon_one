#!/usr/local/bin/php
<?php
/**
 * Copyright 2024 Paul Kraus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

/*
 * Script to fetch member info from the registration DB and populate the virtcon DB.
 * This also updates records where the member changed something on the reg side.
 * Based on code originally written by Paul Kraus and then heavily added to by Mike Rafferty.
 */

/*
define('SCRIPT_PATH', dirname($_SERVER['SCRIPT_FILENAME']));
define('SCRIPT_NAME', substr(basename($_SERVER['SCRIPT_FILENAME']), 0, -4));
define('SCRIPT_PATH_PARENT', dirname($_SERVER['SCRIPT_FILENAME'], 2));
define('SCRIPT_PATH_ROOT', dirname($_SERVER['SCRIPT_FILENAME'], 3));
*/
const SCRIPT_PATH = "/home/virtcon/boskone/2024";
const SCRIPT_PATH_ROOT = "/home/virtcon";
const SCRIPT_NAME = "fetch_reg";
const EMAIL_FLAG = "t";

/*
 * Flags for enhanced logging (VERBOSE), and diagnostics (DEBUG)
 */
if ( !defined('DEBUG') ) define('DEBUG', false);
if ( !defined('VERBOSE') ) define('VERBOSE', false);
if ( !defined('TEST') ) define('TEST', false);

require_once SCRIPT_PATH . "/db_config";
const LOG_DIR = SCRIPT_PATH_ROOT . "/logs";
ini_set('log_errors', true);
ini_set('error_log', LOG_DIR . "/" . SCRIPT_NAME . ".log");
const LOG_FILE = LOG_DIR . "/" . SCRIPT_NAME . ".log";

// who am i
define('MY_PID', getmypid());

/* includes ******************************************************************/
$inc_path = SCRIPT_PATH_ROOT . "/include";
$inc_files_a = scandir($inc_path);
foreach ($inc_files_a as $inc_file) {
	if ( isset($suffix) ) {
		unset($suffix);
	}
	$suffix = substr("$inc_file", -4, 4);
	if ( "$suffix" == ".php" ) {
		require_once "{$inc_path}/{$inc_file}";
	}
}
/*****************************************************************************/
write_log("INFO", "B61", SCRIPT_NAME . " starting");

/*
 * Open a connection to the virtualCon database, we can't do anything without that connection
 */
$virtcon_dbx = connect_db(VIRTCON_DB_USER, VIRTCON_DB_PASS, VIRTCON_DB_NAME, VIRTCON_DB_HOST);
if ( $virtcon_dbx === false ) {
	write_log("ERROR", "B61", "Failed to open virtCon DB connection");
	close_out();
}
$reg_dbx = connect_db(REG_DB_USER, REG_DB_PASS, REG_DB_NAME, REG_DB_HOST);
if ( $reg_dbx === false ) {
	write_log("ERROR", "B61", "Failed to open reg DB connection");
	close_out();
}

$reg_query = "SELECT R.id,
	 		M.label,
	 		P.first_name as Pf,
	 		P.middle_name as Pm,
	 		P.last_name as Pl,
			P.email_addr as Pe,
			P.badge_name as Pb,
			P.id as Pi,
			N.first_name as Nf,
			N.middle_name as Nm,
			N.last_name as Nl,
			N.email_addr as Ne,
			N.badge_name as Nb,
			N.id as Ni,
			R.change_date as reg_date,
			P.update_date as per_date
			FROM reg as R
			JOIN memList as M on M.id=R.memId
			LEFT JOIN perinfo as P on P.id=R.perid
			LEFT JOIN newperson as N on N.id=R.newperid
			WHERE R.conid=" . REG_CON_ID . ";";
//				and M.memCategory!='cancel'";
// 				and R.paid=R.price;";

write_log("INFO", "B61", "Reg Query == $reg_query");

$timestampR = mysqli_query($virtcon_dbx, "SELECT max(reg_timestamp) as max_reg FROM member_info WHERE conv_id=" . CONV_ID . ";");
if ( $timestampR === false ) {
	write_log("ERROR", "B61", "Failed to read from virtcon_dbx connection");
}
$timestampL = mysqli_fetch_assoc($timestampR);
$timestamp = $timestampL['max_reg'];

write_log("INFO", "B61", "Max timestamp from VirtCon member_info = $timestamp");

//echo "BadgeId,Label,First,Middle,Last,Email,BadgeName,Update" . "\n";

$running_count = 0;
$new_count = 0;
$update_count = 0;
$db_fail_count = 0;
$missing_id_count = 0;
$timestamp_count = 0;
$missing_email_count = 0;
$spam_count = 0;
$test_count = 0;

$reportR = mysqli_query($reg_dbx, $reg_query);
if ( VERBOSE ) write_log("VERBOSE", "B61", "Count New Update DB Time ID Email Spam");
if ( $reportR === false ) {
	write_log("ERROR", "B61", "Failed to read from reg_dbx connection");
} else if ( $reportR === null ) {
	write_log("WARN", "B61", "Reg query returned no results");
} else while ( $reportL = mysqli_fetch_assoc($reportR) ) {
	$running_count++;
	if ( !is_null($reportL['Pi']) && $reportL['Pi'] != '' ) {
		$reportL['first'] = $reportL['Pf'];
		$reportL['middle'] = $reportL['Pm'];
		$reportL['last'] = $reportL['Pl'];
		$reportL['email'] = strtolower($reportL['Pe']);
		$reportL['badge'] = $reportL['Pb'];
	} else if ( !is_null($reportL['Ni']) && $reportL['Ni'] != '' ) {
		$reportL['first'] = $reportL['Nf'];
		$reportL['middle'] = $reportL['Nm'];
		$reportL['last'] = $reportL['Nl'];
		$reportL['email'] = strtolower($reportL['Ne']);
		$reportL['badge'] = $reportL['Nb'];
	} else {
		if ( DEBUG ) echo "Skipping due to missing ID:\n";
		if ( DEBUG ) print_r($reportL);
		$missing_id_count++;
		continue;
	}

	$reportL['update'] = max($reportL['reg_date'], $reportL['per_date']);

	if ( $reportL['update'] <= $timestamp ) {
		if ( DEBUG ) echo "Skipping due to timestamp:\n";
		if ( DEBUG ) print_r($reportL);
		$timestamp_count++;
		continue;
	}
	if ( $reportL['first'] == $reportL['last'] ) { //SPAM
		write_log("INFO", "B61", "Skipping potential SPAM entry");
		write_log("SPAM", "B61", '"' . $reportL['id'] . '",'
			. '"' . $reportL['label'] . '",'
			. '"' . $reportL['first'] . '",'
			. '"' . $reportL['middle'] . '",'
			. '"' . $reportL['last'] . '",'
			. '"' . $reportL['email'] . '",'
			. '"' . $reportL['badge'] . '",'
			. '"' . $reportL['update'] . '",'
			. '","' . $reportL['Pi'] . '","' . $reportL['Ni']
			. "\"\n");
		$spam_count++;
		if ( DEBUG ) echo "Skipping due to SPAM:\n";
		if ( DEBUG ) print_r($reportL);
		continue;
	}

	if ( $reportL['email'] == "" ) {
		write_log("INFO", "B61", "Missing Email for badge " . $reportL['id'] . ".");
		if ( DEBUG ) echo "Skipping due to missing email:\n";
		if ( DEBUG ) print_r($reportL);
		$missing_email_count++;
		continue;
	}
	$new_guid = uuid_create(UUID_TYPE_RANDOM);
	$testQ = mysqli_query($virtcon_dbx, "SELECT email FROM member_info WHERE guid='$new_guid';");
	while ( $testQ === null ) {
		write_log("WARN", "B61", "GUID Collision: $new_guid");
		$new_guid = uuid_create(UUID_TYPE_RANDOM);
		$testQ = mysqli_query($virtcon_dbx, "SELECT email FROM member_info WHERE guid='$new_guid';");
	}

	$in_or_up_q = "INSERT INTO member_info (conv_id, email, guid, reg_timestamp, name_badge, name_first, name_last) VALUES (" . CONV_ID . ","
		. "'" . mysqli_escape_string($virtcon_dbx, $reportL['email']) . "','$new_guid',"
		. "'" . mysqli_escape_string($virtcon_dbx, $reportL['update']) . "',"
		. "'" . mysqli_escape_string($virtcon_dbx, $reportL['badge']) . "',"
		. "'" . mysqli_escape_string($virtcon_dbx, $reportL['first']) . "',"
		. "'" . mysqli_escape_string($virtcon_dbx, $reportL['last']) . "')"
		. " ON DUPLICATE KEY UPDATE "
		. "name_badge='" . mysqli_escape_string($virtcon_dbx, $reportL['badge']) . "',"
		. "name_first='" . mysqli_escape_string($virtcon_dbx, $reportL['first']) . "',"
		. "name_last='" . mysqli_escape_string($virtcon_dbx, $reportL['last']) . "';";

	if ( TEST === true ) {
		/*
		 * Since we are in TEST mode, do not actually update the VirtCon DB, just echo what we would have done.
		 */
		echo "$in_or_up_q\n";
		$test_count++;
	} else {
		$insert_result = mysqli_query($virtcon_dbx, $in_or_up_q);
		if ( $insert_result === false ) {
			write_log("ERROR", "B61", "Failed to write to virtcon_dbx connection: " . $reportL['id']);
			if ( DEBUG ) echo "$in_or_up_q\n";
			$db_fail_count++;
			continue;
		}
		if ( EMAIL_FLAG == "t" ) {
			/*
			 * We have the EMAIL_FLAG so we need to send a welcome email to this new registration
			 */
			if ( VERBOSE ) write_log('VERBOSE', MY_PID, "Sending email to $reportL[email] for $new_guid");
			$email_Status = send_email("$new_guid", true);
			if ( VERBOSE ) write_log('VERBOSE', MY_PID, "Email result == $email_Status");
			if ( $email_Status === false ) write_log('WARN', MY_PID, "Failed to send email to $reportL[email] for $new_guid");
			if ( $email_Status === true ) update_flag($virtcon_dbx, $new_guid, "c_flag_1", 1);
		}
		$insert_count = mysqli_affected_rows($virtcon_dbx);
		if ( $insert_count == 1 ) {
			$new_count++;
		}
		if ( $insert_count == 2 ) {
			$update_count++;
		}
	}
	if ( VERBOSE ) write_log("VERBOSE", "B61", "(" . SCRIPT_NAME . ") $new_count $update_count $db_fail_count $timestamp_count $missing_id_count $missing_email_count $spam_count");
}
if ( isset($reg_dbx) ) {
	if ( $reg_dbx !== false ) {
		mysqli_close($reg_dbx);
	}
}
if ( isset($virtcon_dbx) ) {
	if ( $virtcon_dbx !== false ) {
		mysqli_close($virtcon_dbx);
	}
}
if ( TEST === true ) {
	write_log("INFO", "B61", "(" . SCRIPT_NAME . ") ending test count = $test_count");
} else {
	write_log("INFO", "B61", "(" . SCRIPT_NAME . ")           new: $new_count");
	write_log("INFO", "B61", "(" . SCRIPT_NAME . ")       updated: $update_count");
	write_log("INFO", "B61", "(" . SCRIPT_NAME . ")       db fail: $db_fail_count");
	write_log("INFO", "B61", "(" . SCRIPT_NAME . ")     timestamp: $timestamp_count");
	write_log("INFO", "B61", "(" . SCRIPT_NAME . ")    missing ID: $missing_id_count");
	write_log("INFO", "B61", "(" . SCRIPT_NAME . ") missing email: $missing_email_count");
	write_log("INFO", "B61", "(" . SCRIPT_NAME . ") possible SPAM: $spam_count");
}
exit();
?>
