<?php
/**
 * Copyright 2024 Paul Kraus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

/*
 * Command line / CRON script to fetch any new registrations from the registration system and create users
 * and GUIDs for them in the virtCon database. It is up the convention administrators to promote
 * additional rights / roles beyond the basic for the members.
 */

ini_set('display_errors', true);

// where do we start
$script_path = dirname($argv['0']);
$script_name = substr(basename($argv['0']), 0, -4);

// Default configuration (required)
require_once("{$script_path}/virtcon.config");
// Optional configuration (overrides the default above)
if ( is_file("{$script_path}/{$script_name}.config") ) {
	include_once("{$script_path}/{$script_name}.config");
}

/*
 * The following needs to go here and not in a config file because we need the script name
 */
ini_set('log_errors', true);
ini_set('error_log', "{$log_dir}/{$script_name}_php.log");
ini_set('session.use_strict_mode', '1');
$log_file = "{$log_dir}/{$script_name}.log";
$log_db_file = "{$log_dir}/{$script_name}-db.log";

// who am i
$mypid = getmypid();

/* includes ******************************************************************/
$inc_path = "{$script_path}/include";
$inc_files_a = scandir($inc_path);
foreach ($inc_files_a as $inc_file) {
	if ( isset($suffix) ) {
		unset($suffix);
	}
	$suffix = substr("$inc_file", -4, 4);
	if ( "$suffix" == ".php" ) {
		require_once "{$inc_path}/{$inc_file}";
	}
}
/*****************************************************************************/
write_log("INFO", MY_PID, "{$script_name} starting");

// TODO: abstract the reg DB connection on a per-convention basis and set the conv ID
$host = "127.0.0.1";
$user = "albacon";
$password = "AhzN5FrT3fU7X464";
$database = "wp_albacon_2020";
$port = 13306;
$conv_id = 1;

$new_count = 0;

$reg_dbx = mysqli_connect("$host", "$user", "$password", "$database", $port);
if ( $reg_dbx === false ) {
	echo "Failed to open connection to Reg DB\n";
	write_log("ERROR", MY_PID, "Failed to open connectio to Reg DB");
	exit(1);
}
$virtcon_dbx = connect_db("$virt_db_user", "$virt_db_pass", "$virt_db_name", "$virt_db_host");
/*
 * Pull data from the reg DB
 */
/*
 * Determine the latest reg we already have
 */
$search_max_date_sql = "select max(reg_timestamp) from member_info where conv_id = '{$conv_id}';";
$search_max_date_result = mysqli_query($virtcon_dbx, "$search_max_date_sql");
if ( $search_max_date_result === false ) {
	write_log("ERROR", MY_PID, "Max Date query failed");
} else {
	$search_max_date_list = mysqli_fetch_assoc($search_max_date_result);
	if ( $search_max_date_list === null ) {
		write_log("ERROR", MY_PID, "Could not find Max Date");
	} else {
		/*
		 * fetch the latest date from the member_info and use that to set a lower bound
		 * on create_date for data from the registration system
		 */
		$virt_member_max_timestamp = $search_max_date_list['max(reg_timestamp)'];
		$reg_sql = "SELECT entry_id, meta_key, meta_value, date_created FROM wp_frmt_form_entry_meta WHERE date_created > '$virt_member_max_timestamp' AND (meta_key = 'name-1' OR meta_key = 'text-1' OR meta_key = 'email-1');";
		$reg_result = mysqli_query($reg_dbx, "$reg_sql");
		if ( $reg_result === false ) {
			echo "Reg DB query failed\n";
			write_log("ERROR", MY_PID, "Reg DB query failed");
		} else {
			if ( $reg_result === null ) {
				echo "Reg query returned no results\n";
				write_log("WARN", MY_PID, "Reg query returned no results");
			} else {
				/*
				 * Now parse the result into the array fields we need
				 * name_first
				 * name_last
				 * name_badge
				 * email
				 */
				/*
				 * I am adding a SPAM check here, if the First Name and Last Name fields match, then this is probably
				 * SPAM, at least so far all that have met that criteria are SPAM and all the SPAM meets that criteria.
				 * I suspect the bots are just filling in the *Name* fields with their fake name string, since we
				 * have separate First and Last Name fields, they get filled the same.
				 *
				 * Note that I am avoiding ReCaptcha on the Reg end due to issues some of our committee have raised with
				 * it regarding sight impaired people.
				 */
				while ( ($reg_list = mysqli_fetch_assoc($reg_result)) !== null ) {
					switch ($reg_list['meta_key']) {
						case "name-1":
							$name_object = unserialize($reg_list['meta_value']);
							$reg_info_list[$reg_list['entry_id']]['name_first'] = mysqli_real_escape_string($virtcon_dbx, "{$name_object['first-name']}");
							$reg_info_list[$reg_list['entry_id']]['name_last'] = mysqli_real_escape_string($virtcon_dbx, "{$name_object['last-name']}");
							if ( $reg_info_list[$reg_list['entry_id']]['name_first'] == $reg_info_list[$reg_list['entry_id']]['name_last'] ) {
								$reg_info_list[$reg_list['entry_id']]['spam_flag'] = true;
							}
							break;
						case "text-1":
							$reg_info_list[$reg_list['entry_id']]['name_badge'] = mysqli_real_escape_string($virtcon_dbx, "{$reg_list['meta_value']}");
							break;
						case "email-1":
							$reg_info_list[$reg_list['entry_id']]['email'] = mysqli_real_escape_string($virtcon_dbx, "{$reg_list['meta_value']}");
							$reg_info_list[$reg_list['entry_id']]['timestamp'] = mysqli_real_escape_string($virtcon_dbx, "{$reg_list['date_created']}");
							break;
					}
				}
			}
		}
		if ( isset($reg_dbx) ) {
			if ( $reg_dbx !== false ) {
				mysqli_close($reg_dbx);
			}
		}
		if ( isset($reg_info_list) ) {
			foreach ($reg_info_list as $registration_list) {
				/*
				 * If the spam_flag is set for this entry, then skip it altogether and log it
				 */
				if ( isset($registration_list['spam_flag']) ) {
					if ( $registration_list['spam_flag'] === true ) {
						echo "Found a SPAM entry\n";
						print_r($registration_list);
						write_log("INFO", MY_PID, "Skipping potential SPAM entry");
						foreach ($registration_list as $key => $value) {
							write_log("SPAM", MY_PID, "{$key} => {$value}");
						}
					}
				} else {
					/*
					 * Now look for these registrations in the virtCon member_info table and if we do not have this one,
					 * create a new GUID and populate the member_info table
					 */
					$search_sql = "select guid from member_info where email = '{$registration_list['email']}' and name_first = '{$registration_list['name_first']}' and name_last = '{$registration_list['name_last']}' and name_badge = '{$registration_list['name_badge']}'";
					$search_result = mysqli_query($virtcon_dbx, "$search_sql");
					if ( mysqli_num_rows($search_result) == 0 ) {
						/*
						 * Since we did not find this registration we need to create the GUID and populate the reg DB
						 */
						$new_guid = uuid_create(UUID_TYPE_RANDOM);
						$insert_sql = "insert into member_info (guid, conv_id, reg_timestamp, email,name_badge, name_first, name_last, flag_admin, flag_program, flag_committee, flag_staff, flag_tech) VALUES ('{$new_guid}','{$conv_id}','{$registration_list['timestamp']}','{$registration_list['email']}','{$registration_list['name_badge']}','{$registration_list['name_first']}','{$registration_list['name_last']}',0,0,0,0,0)";
						file_put_contents("$log_db_file", "{$insert_sql}\n", FILE_APPEND | LOCK_EX);
						$insert_result = mysqli_query($virtcon_dbx, "$insert_sql");
						if ( $insert_result === false ) {
							write_log("ERROR", MY_PID, "DB Insert failed");
							write_log("ERROR", MY_PID, "{$insert_sql}");
						} else {
							write_log("INFO", MY_PID, "Added {$registration_list['email']} to member info");
							$new_count++;
							send_email("$new_guid", true);
						}
					} else {
						write_log("INFO", MY_PID, "Already have a registration for {$registration_list['email']} so skipping");
					}
				}
			}
		}
	}
}
if ( isset($virtcon_dbx) ) {
	if ( $virtcon_dbx !== false ) {
		mysqli_close($virtcon_dbx);
	}
}
echo "Added {$new_count} members\n";
write_log("INFO", MY_PID, "{$script_name} ending");
