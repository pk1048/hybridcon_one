<?php
/**
 * Copyright 2024 Paul Kraus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

/*
 * Script to handle basic reporting
 */
ini_set('display_errors', false);

// where do we start
define('SCRIPT_PATH', dirname($_SERVER['SCRIPT_FILENAME']));
define('SCRIPT_NAME', substr(basename($_SERVER['SCRIPT_FILENAME']), 0, -4));
define('SCRIPT_PATH_PARENT', SCRIPT_PATH);

// Default configuration (required)
require_once(SCRIPT_PATH_PARENT . "/virtcon.config");
// Database configuration
// if ( is_file("$db_configuration_path/db_config") ) include_once("$db_configuration_path/db_config");
// if ( is_file("$script_path_parent/db_config") ) include_once("$script_path_parent/db_config");

/*
 * Flags for enhanced logging (VERBOSE), diagnostics (DEBUG), and test mode (TEST)
 */
if ( !defined('DEBUG') ) define('DEBUG', false);
if ( !defined('VERBOSE') ) define('VERBOSE', false);
if ( !defined('TEST') ) define('TEST', false);

/*
 * The following needs to go here and not in a config file because we need the script name
 */
if ( !defined('LOG_DIR') ) define('LOG_DIR', "$log_dir");
ini_set('log_errors', true);
ini_set('error_log', LOG_DIR . "/" . SCRIPT_NAME . "/" . SCRIPT_NAME . "_.log");
ini_set('session.use_strict_mode', '1');
define('LOG_FILE', LOG_DIR . "/" . SCRIPT_NAME . ".log");

// who am i
define('MY_PID', getmypid());

$output_file = SCRIPT_PATH . "/report_" . MY_PID . ".csv";

/* includes ******************************************************************/
$inc_path = SCRIPT_PATH_PARENT . "/include";
$inc_files_a = scandir($inc_path);
foreach ($inc_files_a as $inc_file) {
	if ( isset($suffix) ) unset($suffix);
	$suffix = substr("$inc_file", -4, 4);
	if ( "$suffix" == ".php" ) require_once "{$inc_path}/{$inc_file}";
}
/*****************************************************************************/
write_log("INFO", MY_PID, "Starting");

/*
 * Open a connection to the virtualCon database, we can't do anything without that connection
 */
$virtcon_dbx = connect_db(VIRTCON_DB_USER, VIRTCON_DB_PASS, VIRTCON_DB_NAME, VIRTCON_DB_HOST, VIRTCON_DB_PORT);
if ( $virtcon_dbx === false ) {
	write_log("ERROR", MY_PID, "Failed to open VirtCon DB connection");
	//TODO Gracefully exit here
}

if ( is_file($output_file) ) unlink($output_file);
$output_file_h = fopen($output_file, 'a');

$report_sql = "select
    				page_track.member_id as 'badge name',
    				count(page_track.member_id) as 'access count'
    				from page_track
    				where page_track.conv_id = 7
    				group by page_track.member_id
    				order by count(member_id) desc;";

$report_result = mysqli_query($virtcon_dbx, $report_sql);
while ( $report_result_list = mysqli_fetch_assoc($report_result) ) {
	$badge = $report_result_list['badge name'];
	$count = $report_result_list['access count'];
	$email_sql = "select
    				member_info.email
    				from member_info
    				where
        				member_info.conv_id = 7
        				and
        				member_info.name_badge = '$badge';";
	$email_result = mysqli_query($virtcon_dbx, $email_sql);
	$email_list = mysqli_fetch_assoc($email_result);
	$email = $email_list['email'];
	echo "$badge,$email,$count\n";
	$report_data_list = array($badge, $email, $count);
	fputcsv($output_file_h, $report_data_list, ',', '"');
}

fclose($output_file_h);

close_out();
