<?php
/**
 * Copyright 2024 Paul Kraus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

/*
 * Email related functions
 */

/**
 * @param $guid
 * @param false $admin
 * @return bool
 *
 * Send an email to a member with their convention authorization link.
 */

function send_email($guid, $admin = false)
{

	global $script_path_parent;
	global $mypid, $virtcon_dbx;

	if ( $admin === false ) {
		if ( isset($_SESSION['member_email']) ) {
			$email_to = "$_SESSION[member_email]";
		}
		if ( isset($_SESSION['conv_id']) ) {
			$conv_id = "$_SESSION[conv_id]";
		}
	}
	if ( !isset($email_to) || !isset($conv_id) ) {
		$email_sql = "select email, conv_id from member_info where guid = '{$guid}';";
		$email_result = mysqli_query($virtcon_dbx, "$email_sql");
		if ( $email_result === false ) {
			write_log("ERROR", "{$mypid}", "Failed to query DB for {$guid} in order to send email");
		} else {
			$email_list = mysqli_fetch_assoc($email_result);
			$email_to = $email_list['email'];
			$conv_id = $email_list['conv_id'];
		}
	}
	if ( isset($email_to) && isset($conv_id) ) {
		$conv_info_list = db_lookup_conv($virtcon_dbx, $conv_id);
		$conv_name = $conv_info_list['name'];
		$conv_year = $conv_info_list['year'];
		$conv_link = "https://{$conv_info_list['virt_server']}/index.php";
		$conv_auth_link = "{$conv_link}?auth={$guid}";
		$member_info_list = db_member_lookup($virtcon_dbx, "$guid");
		/*
		 * Use the per-convention message if it exists
		 */
		if ( isset($conv_name) && isset($conv_year) ) {
			if ( is_file("{$script_path_parent}/$conv_name/$conv_year/email_message.php") ) {
				include "{$script_path_parent}/$conv_name/$conv_year/email_message.php";
			} else {
				include "{$script_path_parent}/default/email_message.php";
			}
		} else {
			$email_message_text = "This is a test, this is only a test, it is only going to a small number of people, DO NOT PANIC!!!\n\r\n\r";
		}

		$email_header_list['From'] = $conv_info_list['reg_email'];
		$mail_status = mail("$email_to", "$email_subject", "$email_message_text", $email_header_list, "");
		usleep(10000);
		if ( $mail_status ) {
			write_log("INFO", MY_PID, "Sent email to {$email_to} for {$conv_info_list['name_visible']}");
		} else {
			write_log("ERROR", MY_PID, "Failed to send email to {$email_to} for {$conv_info_list['name_visible']}");
		}

	} else {
		write_log("ERROR", MY_PID, "Missing either EMAIL or CONV_ID to send email to {$email_to}");
	}
	if ( isset($mail_status) ) {
		$return = $mail_status;
	} else {
		$return = false;
	}

	return $return;
}

/**
 * @param $conv_id
 *
 * The Send Email menu item
 */
function manage_email($conv_id)
{

	global $mypid, $virtcon_dbx;

	if ( !isset($_REQUEST['type']) ) {
		/*
		 * We don't know what to do yet
		 */
		?>
        <p>From this page you can send Convention Authorization Link email to:</p>
        <ol>
            <li>Members based on role</li>
            <li>Individual members (NOT YET)</li>
        </ol>
        <div class="section">
            <h4>Send Convention Authorization Link to Members with a specific Role</h4>
            <p>You may also select a c_flag to include or exclude.</p>
            <form method="post">
                <input type="hidden" name="page" value="email">
                <input type="hidden" name="type" value="role">
                <select name="role">
                    <option value="admin" selected>Admin</option>
                    <option value="committee">Committee</option>
                    <option value="staff">Staff</option>
                    <option value="program">Program Participants</option>
                    <option value="members">All Members</option>
                </select>
                <br>
                <select name="c_flag_value">
                    <option value="c_flag_1" selected>c_flag_1</option>
                    <option value="c_flag_2">c_flag_2</option>
                    <option value="c_flag_3">c_flag_3</option>
                    <option value="c_flag_4">c_flag_4</option>
                </select>
                <br>
                <input type="radio" id="c_flag_none" name="c_flag" value="none" checked>
                <label for="c_flag_none">None</label>
                <br>
                <input type="radio" id="c_flag_inc" name="c_flag" value="include">
                <label for="c_flag_inc">Include</label>
                <br>
                <input type="radio" id="c_flag_exc" name="c_flag" value="exclude">
                <label for="c_flag_exc">Exclude</label>
                <br>
                <input type="submit" value="Send Email to this Role">
            </form>
        </div>
		<?php
		/*
		<div class="section">
			<h4>Send Convention Authorization Link to a specific Member</h4>
			<p>Enter email address or name (first, last, or badge) below:</p>
			<form method="post">
				<input type="hidden" name="page" value="email">
				<input type="hidden" name="type" value="member">
				<input type="text" size="40" maxlength="40" name="search">
				<input type="submit" value="Find Member(s)">
			</form>
		</div>
		*/
	} else {
		$conv_sql = "SELECT guid, email FROM member_info WHERE conv_id = '{$conv_id}'";
		/*
		 * This is where we actually send the email
		 */
		switch ($_REQUEST['type']) {
			case "role":
				if ( isset($_REQUEST['role']) ) {
					switch ($_REQUEST['role']) {
						case "admin":
							$role_sql = "flag_admin = '1'";
							break;
						case "committee":
							$role_sql = "flag_committee = '1'";
							break;
						case "staff":
							$role_sql = "flag_staff = '1'";
							break;
						case "program":
							$role_sql = "flag_program = '1'";
							break;
						case "members":
							$role_sql = "";
							break;
					}
				}
		}
		if ( isset($_REQUEST['c_flag']) && isset($_REQUEST['c_flag_value']) ) {
			$c_flag_value = $_REQUEST['c_flag_value'];
			switch ($_REQUEST['c_flag']) {
				case "none":
					if ( $role_sql != "" ) {
						$email_sql = "{$conv_sql} AND {$role_sql};";
					} else {
						$email_sql = "{$conv_sql};";
					}
					break;
				case "include":
					if ( $role_sql !== "" ) {
						$email_sql = "{$conv_sql} AND ( {$role_sql} OR {$c_flag_value} = '1' );";
					} else {
						$email_sql = "{$conv_sql} AND {$c_flag_value} = '1';";
					}
					break;
				case "exclude":
					if ( $role_sql !== "" ) {
						$email_sql = "{$conv_sql} AND ( {$role_sql} AND {$c_flag_value} != '1' );";
					} else {
						$email_sql = "{$conv_sql} AND {$c_flag_value} != '1';";
					}
					break;
			}
		}
		if ( isset($email_sql) ) {
			$email_result = mysqli_query($virtcon_dbx, "$email_sql");
			if ( $email_result === false ) {
				echo "<p>The DB query for {$_REQUEST['role']} members failed.</p>";
			} else {
				$email_count = mysqli_num_rows($email_result);
				if ( $email_count == 0 ) {
					echo "<p>Could not find any members that met the criteria.</p>";
				} else {
					echo "<p>Sending the following Convention Authorization Link messages to {$email_count} members.</p>\n";

					while ( ($email_list = mysqli_fetch_assoc($email_result)) !== null ) {
						$email_address = $email_list['email'];
						$guid = $email_list['guid'];
						send_email("{$guid}", true);
					}
				}
			}
		} else {
			echo "<p>Something went wrong, please start over</p>";
		}
	}
}

