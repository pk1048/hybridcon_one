<?php
/**
 * Copyright 2024 Paul Kraus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

/*
 * Miscellaneous functions
 */

/**
 * @param $data
 * @return string|void
 * @throws Exception
 *
 * If we do not have the PECL extension for uuid_create() we need an alternate
 */
function guidv4($data = null)
{
	// Generate 16 bytes (128 bits) of random data or use the data passed into the function.
	$data = $data ?? random_bytes(16);
	assert(strlen($data) == 16);

	// Set version to 0100
	$data[6] = chr(ord($data[6]) & 0x0f | 0x40);
	// Set bits 6-7 to 10
	$data[8] = chr(ord($data[8]) & 0x3f | 0x80);

	// Output the 36 character UUID.
	return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
}


/**
 * @return false|string
 */
function now()
{
	$now = date('U');

	return $now;
}

/**
 *
 */
function not_registered()
{
	global $virtcon_dbx;

	if ( VERBOSE ) write_log("VERBOSE", MY_PID, "Entering NOT REGISTERED");

	$request_server = $_SERVER['SERVER_NAME'];
	$conv_info_list = db_match_request($virtcon_dbx, $request_server);

	if ( $conv_info_list === false ) {
		write_log("WARN", MY_PID, "Could not find this convention {$request_server}");
		echo "<p>Sorry, we cannot help you as we are not sure what you are trying to do. Please double check the URL (link) and try again.</p>\n";
	} else {
		$conv_reg_url = $conv_info_list['reg_url'];
		$conv_name = $conv_info_list['name_visible'];
		echo "<div class='warn'>";
		echo "<p>Let's get you logged in!</p>\n";
		echo "<p>If you have not yet registered, <a href='{$conv_reg_url}'>Click here to register for $conv_name</a></p>\n";
		echo "<p>If you have already registered for $conv_name, enter the email address you used to register for $conv_name below and click submit to receive an email with your convention link. Remember to check your Junk/SPAM folder. The email should arrive within a few minutes.</p>\n";
		echo "<form action='index.php' method='get' autocomplete='on'>\n";
		echo "<input type='email' name='email'>\n";
		echo "<input type='submit'>\n";
		echo "</form>\n";
		echo "</div>\n";
	}
	close_out();
}

/**
 * @param $conv_id
 * @param $one_time_code
 */
function process_one_time($conv_id, $one_time_code)
{

	global $virtcon_dbx;

	if ( VERBOSE ) write_log("VERBOSE", MY_PID, "Entering PROCESS ONE TIME");
	$conv_info_list = db_lookup_conv($virtcon_dbx, "$conv_id");
	$conv_name = $conv_info_list['name_visible'];
	/*
	 * Look for $conv_id and $one_time_code in the GUID column of the member_info table
	 */
	if ( check_one_time_code($virtcon_dbx, $conv_id, $one_time_code) !== true ) {
		/*
		 * Let the person know they submitted the wrong $one_time_code or they submitted it
		 * to the wrong convention URL.
		 */
		write_log("BAD OTC", MY_PID, "Could not find the combination of {$conv_id} and {$one_time_code}");
		echo "<div class='error'>";
		echo "<p>We could not find that ONE TIME CODE for $conv_name, please check the URL and the code and try again.</p>\n";
		echo "<p>You entered the following URL {$_SERVER['SERVER_NAME']}:/{$_SERVER['REQUEST_URI']}</p>\n";
		echo "<p>You entered the following ONE TIME CODE {$one_time_code}</p>\n";
		echo "<p>If it still refuses to work for you please contact the help resource below and have your ONE TIME CODE and URL handy.</p>\n";
		echo "<p>{$conv_info_list['help_contact']}</p>\n";
		echo "</div>\n";
	} else {
		/*
		 * Have the member provide their Names (First, Last, Badge) and email address, we create a
		 * temporary GUID and store it in the email field to validate the request when submitted.
		 */
		$otc_guid = uuid_create(UUID_TYPE_RANDOM);
		set_otc_guid($virtcon_dbx, $conv_id, $one_time_code, $otc_guid);
		member_info_form($conv_id, $conv_name, 'otc.php', $otc_guid, $one_time_code);
	}
	/*
	 * When they submit that information it will go to a URL to process that will create a new
	 * row in the member_info table for this membership.
	 * Once that page has processed the information it will redirect
	 * them to the convention URL with the GUID already loaded.
	 */
	close_out();
}

/**
 * @param $request_email
 */
function send_email_reminder($request_email)
{
	global $virtcon_dbx;
	$error_flag = false;

	if ( VERBOSE ) {
		write_log("VERBOSE", MY_PID, "Entering SEND EMAIL REMINDER");
	}

	$request_server = $_SERVER['SERVER_NAME'];
	$conv_info_list = db_match_request($virtcon_dbx, $request_server);
	if ( $conv_info_list === false ) {
		write_log("WARN", MY_PID, "Could not find this convention {$request_server}");
		echo "<p>Sorry, we cannot help you as we are not sure what you are trying to do. Please double check the URL (link) and try again.</p>\n";
	} else {
		$conv_name = $conv_info_list['name_visible'];
		if ( !isset($_SESSION['email_reminder']) && $request_email != 'blarg' ) {
			$conv_id = $conv_info_list['conv_id'];
			$_SESSION['email_reminder']['email'] = $request_email;
			$_SESSION['email_reminder']['conv'] = $conv_id;
			$_SESSION['email_reminder']['guid'] = $email_guid = uuid_create(UUID_TYPE_RANDOM);
			if ( VERBOSE ) {
				write_log("VERBOSE", MY_PID, "Email reminder confirmation for {$request_email}");
			}
			echo "<div class='warn'>";
			echo "<p>You requested a convention link reminder for $conv_name.</p>\n";
			echo "<p>In order to prevent abuse you must click <a href='index.php?email=blarg&email_guid={$email_guid}'>here</a> to complete the request. Remember to check your Junk/SPAM folder</p>\n";
			echo "</div>\n";
		} else {
			if ( isset($_SESSION['email_reminder']['email']) ) {
				$email = $_SESSION['email_reminder']['email'];
			} else {
				$error_flag = true;
			}
			if ( isset($_SESSION['email_reminder']['conv']) ) {
				$conv_id = $_SESSION['email_reminder']['conv'];
			} else {
				$error_flag = true;
			}
			if ( isset($_SESSION['email_reminder']['guid']) ) {
				$email_guid = $_SESSION['email_reminder']['guid'];
			} else {
				$error_flag = true;
			}
			if ( isset($_REQUEST['email_guid']) ) {
				$request_guid = $_REQUEST['email_guid'];
			} else {
				$error_flag = true;
			}
			if ( VERBOSE ) {
				write_log("VERBOSE", MY_PID, "Email reminder process confirmation {$request_email}:{$request_guid}");
			}
			if ( $email_guid == $request_guid ) {
				if ( VERBOSE ) {
					write_log("VERBOSE", MY_PID, "Email reminder process confirmation matching GUID");
				}
				$find_user_guid_sql = "select * from member_info where conv_id = '{$conv_id}' and email = '{$email}'";
				$find_user_guid_result = mysqli_query($virtcon_dbx, $find_user_guid_sql);
				if ( $find_user_guid_result !== false ) {
					if ( mysqli_num_rows($find_user_guid_result) > 0 ) {
						$find_user_guid_list = mysqli_fetch_assoc($find_user_guid_result);
						$user_guid = $find_user_guid_list['guid'];
						if ( VERBOSE ) {
							write_log("VERBOSE", MY_PID, "Sending email for {$user_guid}");
						}
						send_email($user_guid);
					} else {
						if ( VERBOSE ) {
							write_log("VERBOSE", MY_PID, "Query to find the user GUID found 0 rows.");
							write_log("VERBOSE", MY_PID, "SQL = {$find_user_guid_sql}");
						}
					}
				} else {
					if ( VERBOSE ) {
						write_log("VERBOSE", MY_PID, "Query to find the user GUID failed.");
					}
				}
				echo "<div class='warn'>";
				echo "<p>If you registered for $conv_name using the email you provided, then we have emailed you a convention link reminder. If you do not receive a reminder email within the next few minutes please contact support for $conv_name and ask them to check your registration status.</p>\n";
			} else {
				echo "<p>Verification check failed. Please contact support for your convention and give them this code <b>EMAIL GUID AUTH FAILURE</b>.</p>\n";
				echo "</div>\n";
			}
			unset($_SESSION['email_reminder']);
		}
	}
	close_out();
}

/**
 *
 */
function diagnostics()
{
	global $member_info_list, $conv_info_list;

	$show_diag = false;
	/* Diagnostics block shown only to developers ********************************/
	if ( isset($member_info_list) ) {
		if ( $member_info_list['flag_admin'] ) {
			$show_diag = true;
		}
	}
	if ( isset($conv_info_list) ) {
		if ( $conv_info_list['name'] == "DEV" ) {
			$show_diag = true;
		}
	}
	if ( $show_diag == true ) {
		echo "<div class='diag'>";
		echo "<hr>\n";
		echo "<h4>Diagnostics</h4>\n";
		echo "<p>Only visible to Admins</p>\n";
		echo "<hr>\n";
		echo "<h5>Paths</h5>\n";
		echo "<pre>";
		echo 'SCRIPT_NAME == ' . SCRIPT_NAME . "\n";
		echo 'SCRIPT_PATH == ' . SCRIPT_PATH . "\n";
		echo 'SCRIPT_PATH_PARENT == ' . SCRIPT_PATH_PARENT . "\n";
		echo 'SCRIPT_PATH_ROOT == ' . SCRIPT_PATH_ROOT . "\n";
		echo "</pre>\n";
		echo "<hr>\n";
		echo "<h5>Member Info</h5>\n";
		echo "<pre>";
		print_r($member_info_list);
		echo "</pre>\n";
		if ( isset($conv_info_list) ) {
			echo "<hr>\n";
			echo "<h5>Convention Info</h5>\n";
			echo "<pre>";
			print_r($conv_info_list);
			echo "</pre>\n";
		}
		echo "<hr>\n";
		echo "<h5>\$_REQUEST</h5>\n";
		echo "<pre>";
		print_r($_REQUEST);
		echo "</pre>\n";
		echo "<hr>\n";
		if ( isset($_SESSION) ) {
			echo "<h5>\$_SESSION</h5>\n";
			if ( isset($_SESSION['expiration']) ) {
				echo "<p>Session expires: ";
				echo date('c', $_SESSION['expiration']);
				echo "</p>\n";
			}
			echo "<h5>Session Parameters</h5>\n";
			echo "<ul>\n";
			$session_name = session_name();
			echo "<li>Session Name == {$session_name}</li>\n";
			$session_cookie_parameters = session_get_cookie_params();
			echo "<li>Session Cookie Parameters\n";
			echo "<pre>";
			print_r($session_cookie_parameters);
			echo "</pre></li>\n";
			$session_id = session_id();
			echo "<li>Session ID == {$session_id}\n";
			echo "</ul>\n";
			echo "<pre>";
			print_r($_SESSION);
			echo "</pre>\n";
		}
		echo "<h5>\$_SERVER</h5>\n";
		echo "<pre>";
		print_r($_SERVER);
		echo "</pre>\n";
		echo "<hr>\n";
		echo "</div>";
	}
}

/**
 *
 */
function close_out()
{
	if ( isset($_SERVER['SERVER_NAME']) ) {
		diagnostics();
		echo "</div>\n";
		echo "</body>\n";
	}
	if ( isset($virtcon_dbx) ) {
		if ( $virtcon_dbx !== false ) {
			mysqli_close($virtcon_dbx);
		}
	}
	if ( isset($reg_dbx) ) {
		if ( $reg_dbx !== false ) {
			mysqli_close($reg_dbx);
		}
	}
	write_log("INFO", MY_PID, "Ending");
	exit();
}
