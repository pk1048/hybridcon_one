<?php
/**
 * Copyright 2024 Paul Kraus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

/*
 * All things related to SESSIONS
 */

/*
 * We do not do anything with SESSIONS unless we are being called via a web server
 */
if ( isset($_SERVER['REQUEST_SCHEME']) ) {
	/*
	 * There are a number of configuration values we want to set for SESSIONS,
	 * these must be set before session_start();, so they are here and will be processed when the session
	 * functions are included.
	 */
	$seven_days = (60 * 60 * 24 * 7);
	session_name('VIRTCON');
	ini_set('session.gc_probability', 0);
	ini_set('session.gc_maxlifetime', $seven_days);
	ini_set('session.cookie_lifetime', $seven_days);
	ini_set('session.cookie_samesite', 'Strict');

	/**
	 * @param $member_info_list
	 * @param $convention_info_list
	 */

	function setup_session($member_info_list, $convention_info_list)
	{

		/*
		 * If the session expiration is not yet set,
		 * set it to now + 7 days
		 */
		if ( !isset($_SESSION['expiration']) ) {
			$_SESSION['expiration'] = (date('U') + (60 * 60 * 24 * 7));
		}
		/*
		 * User info
		 */
		$_SESSION['guid'] = $member_info_list['guid'];
		$_SESSION['conv_id'] = $member_info_list['conv_id'];
		$_SESSION['email'] = $member_info_list['email'];
		$_SESSION['name_badge'] = $member_info_list['name_badge'];
		$_SESSION['name_first'] = $member_info_list['name_first'];
		$_SESSION['name_last'] = $member_info_list['name_last'];
		$_SESSION['flag_admin'] = $member_info_list['flag_admin'];
		$_SESSION['flag_program'] = $member_info_list['flag_program'];
		$_SESSION['flag_committee'] = $member_info_list['flag_committee'];
		$_SESSION['flag_staff'] = $member_info_list['flag_staff'];
		$_SESSION['flag_tech'] = $member_info_list['flag_tech'];
		/*
		 * Convention info
		 */
		$_SESSION['virt_server'] = $convention_info_list['virt_server'];
		$_SESSION['name_visible'] = $convention_info_list['name_visible'];
		$_SESSION['konopas_path'] = $convention_info_list['konopas_path'];
		$_SESSION['reg_email'] = $convention_info_list['reg_email'];
		$_SESSION['help_contact'] = $convention_info_list['help_contact'];
	}

	/**
	 *
	 */

	function clear_session()
	{

		if ( isset($_SESSION['expiration']) ) {
			unset($_SESSION['expiration']);
		}
		if ( isset($_SESSION['guid']) ) {
			unset($_SESSION['guid']);
		}
		if ( isset($_SESSION['conv_id']) ) {
			unset($_SESSION['conv_id']);
		}
		if ( isset($_SESSION['email']) ) {
			unset($_SESSION['email']);
		}
		if ( isset($_SESSION['name_badge']) ) {
			unset($_SESSION['name_badge']);
		}
		if ( isset($_SESSION['name_first']) ) {
			unset($_SESSION['name_first']);
		}
		if ( isset($_SESSION['name_last']) ) {
			unset($_SESSION['name_last']);
		}
		if ( isset($_SESSION['flag_admin']) ) {
			unset($_SESSION['flag_admin']);
		}
		if ( isset($_SESSION['flag_program']) ) {
			unset($_SESSION['flag_program']);
		}
		if ( isset($_SESSION['flag_committee']) ) {
			unset($_SESSION['flag_committee']);
		}
		if ( isset($_SESSION['flag_staff']) ) {
			unset($_SESSION['flag_staff']);
		}
		if ( isset($_SESSION['flag_tech']) ) {
			unset($_SESSION['flag_tech']);
		}
		if ( isset($_SESSION['virt_server']) ) {
			unset($_SESSION['virt_server']);
		}
		if ( isset($_SESSION['name_visible']) ) {
			unset($_SESSION['name_visible']);
		}
		if ( isset($_SESSION['konopas_path']) ) {
			unset($_SESSION['konopas_path']);
		}
		if ( isset($_SESSION['reg_email']) ) {
			unset($_SESSION['reg_email']);
		}
		if ( isset($_SESSION['help_contact']) ) {
			unset($_SESSION['help_contact']);
		}
	}
}
