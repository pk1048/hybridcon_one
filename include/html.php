<?php
/**
 * Copyright 2024 Paul Kraus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

/*
 * Collection of HTML functions
 */

/**
 * @param string $target
 */

/**
 * @param string $target
 */
function redirect_home($target = "/index.php")
{

	?>
    <!DOCTYPE html>
    <html>
    <head>
        <meta http-equiv="refresh" content="0" url="<?php echo "{$target}" ?>"/>
    </head>
    <body>
    <p>Something went wrong and you should not see this.</p>
    <p>If you do, please click <a href="<?php echo "{$target}" ?>" here</a></p>
    </body>
    </html>
	<?php

	write_log("REDIRECT", MY_PID, "We just redirected the user to {$target}");
	exit(0);
}

/**
 * @param null $additional_headers
 */
function html_header($additional_headers = null)
{
	?>
    <!DOCTYPE html>
    <html lang='en>
<head><meta charset=' utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0''>

    <!--    <link href="datatables.css" rel="stylesheet"> -->
    <link href="Bootstrap-5-5.3.0/css/bootstrap.min.css" rel="stylesheet">
    <link href="DataTables-1.13.6/css/dataTables.bootstrap5.min.css" rel="stylesheet">
    <link href="Buttons-2.4.1/css/buttons.bootstrap5.min.css" rel="stylesheet">
    <link href="DateTime-1.5.1/css/dataTables.dateTime.min.css" rel="stylesheet">
    <link href="FixedHeader-3.4.0/css/fixedHeader.bootstrap5.min.css" rel="stylesheet">
    <link href="Responsive-2.5.0/css/responsive.bootstrap5.min.css" rel="stylesheet">

    <!--    <script src="datatables.js"></script> -->
    <script src="jQuery-3.7.0/jquery-3.7.0.min.js"></script>
    <script src="Bootstrap-5-5.3.0/js/bootstrap.bundle.min.js"></script>
    <script src="DataTables-1.13.6/js/jquery.dataTables.min.js"></script>
    <script src="DataTables-1.13.6/js/dataTables.bootstrap5.min.js"></script>
    <script src="Buttons-2.4.1/js/dataTables.buttons.min.js"></script>
    <script src="Buttons-2.4.1/js/buttons.bootstrap5.min.js"></script>
    <script src="Buttons-2.4.1/js/buttons.colVis.min.js"></script>
    <script src="Buttons-2.4.1/js/buttons.html5.min.js"></script>
    <script src="Buttons-2.4.1/js/buttons.print.min.js"></script>
    <script src="DateTime-1.5.1/js/dataTables.dateTime.min.js"></script>
    <script src="FixedHeader-3.4.0/js/dataTables.fixedHeader.min.js"></script>
    <script src="Responsive-2.5.0/js/dataTables.responsive.min.js"></script>
    <script src="Responsive-2.5.0/js/responsive.bootstrap5.js"></script>

    <script>$(document).ready(function () {
            $('#member_list').DataTable({
                'order': [0, 'asc'],
                'pageLength': 100,
                'autoWidth': true
            })
        });</script>
    <script>$(document).ready(function () {
            $('#report').DataTable({
                'order': [0, 'asc'],
                'pageLength': 100,
                'autoWidth': true
            })
        });</script>
    <script>$(document).ready(function () {
            $('#film').DataTable({
                'order': [6, 'asc'],
                'pageLength': 10,
                'autoWidth': true
            })
        });</script>
    <script>$(document).ready(function () {
            $('#artist').DataTable({
                'order': [0, 'asc'],
                'pageLength': 10,
                'info': false,
                'lengthChange': false,
                'searching': false,
                'paging': false,
                'autoWidth': true
            })
        });</script>
    <script>$(document).ready(function () {
            $('#dealer').DataTable({
                'order': [0, 'asc'],
                'pageLength': 10,
                'info': false,
                'lengthChange': false,
                'searching': false,
                'paging': false,
                'autoWidth': true
            })
        });</script>
	<?php
	if ( $additional_headers !== null ) {
		echo "$additional_headers";
	}
	echo "</head>\n";
}

/**
 * @param $conv_id
 */
function convention_home()
{
	if ( is_file(SCRIPT_PATH_PARENT . "/" . CONV_NAME . "/" . CONV_YEAR . "/home.html") ) {
		readfile(SCRIPT_PATH_PARENT . "/" . CONV_NAME . "/" . CONV_YEAR . "/home.html");
	} else {
		readfile(SCRIPT_PATH_PARENT . "/default/home.html");
	}
}

/**
 * @param $conv_id
 */
function film_page($conv_id)
{
	?>
    <h3>Albacon Film / Anime Watch Party</h3>
    <h4>What is a Watch Party?</h4>
    <p>A Watch Party is a way to watch movies and tv show with your friends and family, and you never have to leave
        home. There is also a chat room for the viewer to post to comment on the film.</p>
    <h4>What do I need to join the Watch Party?</h4>
    <ol>
        <li>You will need a desktop or laptop that has a Chrome browser.</li>
        <li>Download the chrome extension for Scener.</li>
        <li>Open up the extension for Scener or go to scener.com</li>
        <li>Create a user account.</li>
    </ol>
    <h4>Why do I need to have an account with Netflix and Amazon to watch the videos Albacon is showing?</h4>
    <p>Albacon does not have the screening rights to show theses films. The whole purpose for Watch Parties is to watch
        movies with friends and fellow fans, not to watch movies for free. We are using Netflix and Amazon because they
        are the two most used streaming services.</p>
    <h4>How to enter the Watch Party room?</h4>
    <ol>
        <li>Click on the link in the schedule.</li>
        <li>This will bring you to the Watch Party Room.</li>
        <li>Click the “JOIN” button.</li>
        <li>Close the pop window to turn on camera and Microphone.</li>
        <li>You may need to sign into your Netflix / Amazon account.</li>
        <li>You need to login into Amazon Watch Party</li>
        <li>The show should start.</li>
    </ol>
    <h4>Chat Room</h4>
    <p>You can open and close the chat room. You will see the Scener icon on the task bar. Hoover over the icon and you
        will see two views, a full screen and a chat menu. Click the full screen view to close the chat bar, click the
        chat bar to bring it back.</p>
    <h4>Schedule</h4>
    <table id='film' class='display'>
        <thead>
        <tr>
            <th>Title<br>click to join watch party</th>
            <th>Genre</th>
            <th>Runtime</th>
            <th>Rating</th>
            <th>Source</th>
            <th></th>
            <th>Showtime</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td><a href="https://scener.com/nymanga/3VrHZZOIxKXxMjRiL4d7">The Tomorrow War</a></td>
            <td>Sci-Fi</td>
            <td>138 min</td>
            <td>PG13</td>
            <td>Amazon</td>
            <td>Live Action</td>
            <td>Sep 17 Friday 9:00 PM</td>
        </tr>
        <tr>
            <td><a href="https://scener.com/nymanga/WqlPgnfSHkRaoJUuyA9q">The Last Unicorn</a></td>
            <td>Fantasy</td>
            <td>92 min</td>
            <td>G</td>
            <td>Amazon</td>
            <td>Animation</td>
            <td>Sep 18 Saturday 10:00 AM</td>
        </tr>
        <tr>
            <td><a href="https://scener.com/nymanga/dZXN7nmPk6wa9l83J4FC">Fate Grand Order: first Order</a></td>
            <td>Fantasy</td>
            <td>72 min</td>
            <td>TV14</td>
            <td>Netfilix</td>
            <td>Animation</td>
            <td>Sep 18 Saturday 12:00 PM</td>
        </tr>
        <tr>
            <td><a href="https://scener.com/nymanga/3y9JAJm2CgDlhdIls268">Violet Evergarden</a></td>
            <td>Sci-Fi</td>
            <td>90 min</td>
            <td>TV PG</td>
            <td>Netflix</td>
            <td>Animation</td>
            <td>Sep 18 Saturday 2:00 PM</td>
        </tr>
        <tr>
            <td><a href="https://scener.com/nymanga/qPBHFjLtchZTRXg3BWYd">Evagleon: 3.0+1.01 Thrice Upon a Time</a></td>
            <td>Sci-Fi</td>
            <td>155 min</td>
            <td>16+</td>
            <td>Amazon</td>
            <td>Animation</td>
            <td>Sep 18 Saturday 4:00 PM</td>
        </tr>
        <tr>
            <td><a href="https://scener.com/nymanga/OpvOkHk5HfSS8Dzctiw3">Cherry 2000</a></td>
            <td>Sci-Fi</td>
            <td>98 min</td>
            <td>PG13</td>
            <td>Amazon</td>
            <td>Live Action</td>
            <td>Sep 18 Saturday 7:00 PM</td>
        </tr>
        <tr>
            <td><a href="https://scener.com/nymanga/c1A1k5lENzDYQ91lHeor">Army of the Dead</a></td>
            <td>Horror</td>
            <td>148 min</td>
            <td>R</td>
            <td>Netfilix</td>
            <td>Live Action</td>
            <td>Sep 18 Saturday 9:00 PM</td>
        </tr>
        </tbody>
    </table>

	<?php
}

/**
 * @param $value
 * @param $enabled
 * @param $form_id
 * @param $action_list
 */
function button($value, $enabled, $form_id, $action_list)
{
	if ( $value == 1 ) {
		$button_class = 'button_yes';
		$text = 'Yes';
	} else {
		$button_class = 'button_no';
		$text = 'No';
	}
	if ( $enabled == 1 ) {
		$guid = "{$action_list['guid']}";
		$role = "{$action_list['role']}";
		$grant = "{$action_list['grant']}";
		echo "<form method='post' name='{$form_id}'>";
		echo "<input type='hidden' name='member_guid' value='{$guid}'>";
		echo "<input type='hidden' name='member_role' value='{$role}'>";
		echo "<input type='hidden' name='member_grant' value='{$grant}'>";
		echo "<input class='{$button_class}' type='submit' value='{$text}'>";
//		echo "<button class='{$button_class}' form='{$form_id}' type='submit' formtarget='_self' formmethod='post'>";
//		echo "{$text}";
//		echo "</button>";
		echo "</form>";
	} else {
		echo "<button class='{$button_class}' disabled>";
		echo "{$text}";
		echo "</button>";
	}
}

/**
 * @param $conv_id
 * @param $conv_name
 * @param $submission_url
 * @param $guid
 * @param $one_time_code
 */
function member_info_form($conv_id, $conv_name, $submission_url, $guid, $one_time_code)
{
	echo "<div class='otc'>";
	echo "<p>Please enter the following information to complete your registration for $conv_name</p>\n";
	echo "<form action='{$submission_url}' method='post' autocomplete='on'>\n";
	echo "<input type='hidden' name='conv_id' value='{$conv_id}'>\n";
	if ( $one_time_code === true ) {
		echo "<input type='text' name='otc' placeholder='One Time Code' size='16'>\n";
	} else {
		echo "<input type='hidden' name='otc' value='{$one_time_code}'>\n";
	}
	if ( $guid !== null ) {
		echo "<input type='hidden' name='guid' value='{$guid}'>\n";
	}
	echo "<input type='text' name='first_name' placeholder='First Name (required)' maxlength='32' required>\n";
	echo "<input type='text' name='last_name' placeholder='Last Name (required)' maxlength='32' required>\n";
	echo "<input type='text' name='badge_name' placeholder='Badge Name' maxlength='32'>\n";
	echo "<input type='email' name='email' placeholder='Email Address (required)' maxlength='250' required>\n";
	echo "<input type='submit' value='Register'>\n";
	echo "</form>\n";
	echo "</div>\n";
}
