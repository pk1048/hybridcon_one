<?php
/**
 * Copyright 2024 Paul Kraus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

/**
 * Log writing functions
 */

/**
 * write_log()
 * @param $severity
 * @param $pid
 * @param $message
 */
function write_log($severity, $pid, $message)
{
	$now = date('c');
	if ( !isset($_SERVER['SERVER_NAME']) && DEBUG ) echo "$now $pid $severity $message\n";
	if ( defined('CONV_NAME') ) {
		file_put_contents(LOG_FILE, "$now $pid $severity (" . CONV_NAME . ") $message\n", FILE_APPEND | LOCK_EX);
	} else {
		file_put_contents(LOG_FILE, "$now $pid $severity $message\n", FILE_APPEND | LOCK_EX);
	}
}

/**
 * write_raw()
 * @param $message
 */
function write_raw($message)
{
	file_put_contents(LOG_FILE, "$message\n", FILE_APPEND | LOCK_EX);
}
