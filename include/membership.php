<?php
/**
 * Copyright 2024 Paul Kraus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

/*
 * This function handles display and management of the membership list
 *
 * Everyone can see the Badge name
 * Program and Staff can see First and Last Name
 * Committee and Admin can see the First and Last Name and Email address
 * Committee can add/remove the following roles:
 * 		Staff
 * 		Program
 * 		Tech
 * Admin can add/remove the following roles:
 * 		Committee
 * 		Admin
 */

/**
 * @param $conv_id
 * @return void
 */
function member_list($conv_id)
{

	global $virtcon_dbx, $mypid;
	write_log("INFO", MY_PID, "Starting MEMBER LIST");
	/*
	 * We start by checking authorization and assigning roles, do not assume that because someone got to this page
	 * that they are authorized.
	 */
	if ( !isset($_SESSION['guid']) ) {
		exit(1);
	} else {
		$my_guid = $_SESSION['guid'];
		$role_list = build_role_list();
		/*
		 * Process any member changes submitted
		 */
		if ( isset($_REQUEST['member_guid']) ) {
			/*
			 * We have a member change to process
			 */
			if ( isset($_REQUEST['member_role']) ) {
				/*
				 * So this is a request to change a member's role
				 */
				$permission_denied = true;
				switch ($_REQUEST['member_role']) {
					case "admin":
						if ( $_SESSION['flag_admin'] == 1 ) {
							$permission_denied = false;
						}
						break;
					case "committee":
						if ( $_SESSION['flag_admin'] == 1 ) {
							$permission_denied = false;
						}
						break;
					case "tech":
						if ( $_SESSION['flag_admin'] == 1 ) {
							$permission_denied = false;
						}
						break;
					case "staff":
						if ( $_SESSION['flag_admin'] == 1 || $_SESSION['flag_committee'] == 1 ) {
							$permission_denied = false;
						}
						break;
					case "program":
						if ( $_SESSION['flag_admin'] == 1 || $_SESSION['flag_committee'] == 1 ) {
							$permission_denied = false;
						}
						break;
				}
				if ( $permission_denied === true ) {
					echo "<p class='warn'>You do not have permission to modify the {$_REQUEST['member_role']} role.</p>\n";
				} else {
					db_modify_member_role($virtcon_dbx, "{$_REQUEST['member_guid']}", "{$_REQUEST['member_role']}", "{$_REQUEST['member_grant']}");
					$member_info_list = db_lookup_guid($virtcon_dbx, $my_guid);
					$convention_info_list = db_lookup_conv($virtcon_dbx, $member_info_list['conv_id']);
					setup_session($member_info_list, $convention_info_list);
					$role_list = build_role_list();
				}
			}
		}
		/*
		 * Get convention info
		 */
		$conv_info_list = db_lookup_conv($virtcon_dbx, $conv_id);
		/*
		 * Start building the list of members
		 */
		$member_guid_list = db_member_list($virtcon_dbx, $conv_id);
		if ( $member_guid_list === false ) {
			echo "<p>We could not fetch the list of members right now, please try again later.</p>";
		} else {
			if ( $member_guid_list === null ) {
				echo "<p>We could not fetch the member information right now, please try again later.</p>";
			} else {
				if ( $role_list['committee'] || $role_list['admin'] ) {
					echo "<p>If you do not have permission to change a role that button will be disabled. To add or remove a role from a member, click the button to add the role if they do not have it or remove the role if they do.</p>\n";
				}
				echo "<table id='member_list' class='display'>\n";
				echo "<thead>\n";
				echo "<tr>";
				// Everyone is permitted to see Badge Name
				echo "<th>Badge Name</th>";
				// Roles that are permitted to see First and Last name
				if ( $role_list['staff'] || $role_list['program'] || $role_list['tech'] || $role_list['committee'] || $role_list['admin'] ) {
					echo "<th>First Name</th><th>Last Name</th>";
				}
				// Roles that are permitted to see email, for display purposes we group GUID with email,
				// only tech gets to see GUID
				if ( $role_list['committee'] || $role_list['admin'] ) {
					echo "<th>Email Address</th>";
				} elseif ( $role_list['tech'] ) {
					echo "<th>Email Address<br>GUID Link</th>";
				}
				// Roles that permitted to see roles, we will control ability to change roles on each individual cell
				if ( $role_list['staff'] || $role_list['program'] || $role_list['tech'] || $role_list['committee'] || $role_list['admin'] ) {
					echo "<th>Admin</th><th>Committee</th><th>Tech</th><th>Staff</th><th>Program</th>";
				}
				echo "</tr>\n";
				echo "</thead>\n";
				/*
				 * Now a row per member
				 */
				echo "<tbody>\n";
				foreach ($member_guid_list as $member_guid) {
					$member_info_list = db_member_lookup($virtcon_dbx, $member_guid);
					if ( $member_info_list === false ) {
						echo "<tr><td>We could not fetch member information right now, please try again later. If this problem persists please contact support.</td></tr>\n";
					} else {
						if ( $member_info_list === null ) {
							echo "<tr><td>We were not able to fetch any member information, please try again later. If this problem persists please contact support.</td></tr>\n";
						} else {
							echo "<tr>";
							/*
							 * Column ONE Badge Name
							 */
							echo "<td>{$member_info_list['name_badge']}</td>";
							/*
							 * Columns TWO and THREE First and Last Name
							 */
							if ( $role_list['tech'] || $role_list['committee'] || $role_list['admin'] ) {
								echo "<td>{$member_info_list['name_first']}</td><td>{$member_info_list['name_last']}</td>";
							}
							/*
							 * Column FOUR email (and GUID link)
							 */
							if ( $role_list['committee'] || $role_list['admin'] || $role_list['tech'] ) {
								echo "<td>{$member_info_list['email']}";
								if ( $role_list['tech'] ) {
									$conv_auth_link = "https://{$conv_info_list['virt_server']}/index.php?auth={$member_info_list['guid']}";
									echo "<br>$conv_auth_link";
								}
								echo "</td>";
							}
							/*
							 * Column FIVE - EIGHT roles
							 */
							if ( $role_list['tech'] || $role_list['staff'] || $role_list['program'] || $role_list['committee'] || $role_list['admin'] ) {
								/*
								 * Column 5.1 admin role, only modified by admin
								 */
								echo "<td>";
								if ( $role_list['admin'] ) {
									if ( isset($action_list) ) {
										unset($action_list);
									}
									$action_list['guid'] = "{$member_guid}";
									$action_list['role'] = "admin";
									if ( $member_info_list['flag_admin'] == 0 ) {
										$action_list['grant'] = "1";
									} else {
										$action_list['grant'] = "0";
									}
									button("{$member_info_list['flag_admin']}", 1, "{$member_guid}_{$action_list['role']}", $action_list);
								} else {
									button("{$member_info_list['flag_admin']}", 0, "", "");
								}
								echo "</td>";
								/*
								 * Column 5.2 committee role, only modified by admin
								 */
								echo "<td>";
								if ( $role_list['admin'] ) {
									if ( isset($action_list) ) {
										unset($action_list);
									}
									$action_list['guid'] = "{$member_guid}";
									$action_list['role'] = "committee";
									if ( $member_info_list['flag_committee'] == 0 ) {
										$action_list['grant'] = "1";
									} else {
										$action_list['grant'] = "0";
									}
									button("{$member_info_list['flag_committee']}", 1, "{$member_guid}_{$action_list['role']}", $action_list);
								} else {
									button("{$member_info_list['flag_committee']}", 0, "", "");
								}
								echo "</td>";
								/*
								 * Column 5.3 tech role, only modified by admin
								 */
								echo "<td>";
								if ( $role_list['admin'] ) {
									if ( isset($action_list) ) {
										unset($action_list);
									}
									$action_list['guid'] = "{$member_guid}";
									$action_list['role'] = "tech";
									if ( $member_info_list['flag_tech'] == 0 ) {
										$action_list['grant'] = "1";
									} else {
										$action_list['grant'] = "0";
									}
									button("{$member_info_list['flag_tech']}", 1, "{$member_guid}_{$action_list['role']}", $action_list);
								} else {
									button("{$member_info_list['flag_tech']}", 0, "", "");
								}
								echo "</td>";
								/*
								 * Column 5.4, staff role, may be modified by admin and committee
								 */
								echo "<td>";
								if ( $role_list['admin'] || $role_list['committee'] ) {
									if ( isset($action_list) ) {
										unset($action_list);
									}
									$action_list['guid'] = "{$member_guid}";
									$action_list['role'] = "staff";
									if ( $member_info_list['flag_staff'] == 0 ) {
										$action_list['grant'] = "1";
									} else {
										$action_list['grant'] = "0";
									}
									button("{$member_info_list['flag_staff']}", 1, "{$member_guid}_{$action_list['role']}", $action_list);
								} else {
									button("{$member_info_list['flag_staff']}", 0, "", "");
								}
								echo "</td>";
								/*
								 * Column 5.5, program role, may be modified by admin and committee
								 */
								echo "<td>";
								if ( $role_list['admin'] || $role_list['committee'] ) {
									if ( isset($action_list) ) {
										unset($action_list);
									}
									$action_list['guid'] = "{$member_guid}";
									$action_list['role'] = "program";
									if ( $member_info_list['flag_program'] == 0 ) {
										$action_list['grant'] = "1";
									} else {
										$action_list['grant'] = "0";
									}
									button("{$member_info_list['flag_program']}", 1, "{$member_guid}_{$action_list['role']}", $action_list);
								} else {
									button("{$member_info_list['flag_program']}", 0, "", "");
								}
								echo "</td>";
							}
							echo "</tr>\n";
						}
					}
				}

				echo "</tbody>\n";
				echo "</table>\n";
			}
		}
	}
}

/**
 * @return array
 */
function build_role_list()
{
	if ( $_SESSION['flag_admin'] == 1 ) {
		$role_list['admin'] = true;
	} else {
		$role_list['admin'] = false;
	}
	if ( $_SESSION['flag_committee'] == 1 ) {
		$role_list['committee'] = true;
	} else {
		$role_list['committee'] = false;
	}
	if ( $_SESSION['flag_program'] == 1 ) {
		$role_list['program'] = true;
	} else {
		$role_list['program'] = false;
	}
	if ( $_SESSION['flag_staff'] == 1 ) {
		$role_list['staff'] = true;
	} else {
		$role_list['staff'] = false;
	}
	if ( $_SESSION['flag_tech'] == 1 ) {
		$role_list['tech'] = true;
	} else {
		$role_list['tech'] = false;
	}

	return $role_list;
}
