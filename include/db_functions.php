<?php
/**
 * Copyright 2024 Paul Kraus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

/*
 * Collection of Database functions
 */

/**
 * @param $user
 * @param $password
 * @param $database
 * @param $host
 * @param int $port
 * @return false|mixed|mysqli|null
 */
function connect_db($user, $password, $database, $host, $port = 3306)
{
	$db_cxn = mysqli_connect("$host", "$user", "$password", "$database", $port);
	if ( $db_cxn === false ) {
		write_log("ERROR", MY_PID, "Failed to open connection to database");
		if ( DEBUG ) echo "<pre>Failed to open connection to DB</pre>\n";
		if ( VERBOSE ) write_log("VERBOSE", MY_PID, "mysqli_connect({$host},{$user},xxxxxxxx,{$database},$port)");
	} else {
		if ( VERBOSE ) write_log("VERBOSE", MY_PID, "Connected to DB mysqli_connect({$host},{$user},xxxxxxxx,{$database},$port)");
	}
	return $db_cxn;
}

/**
 * @param $db_cxn
 * @param $guid
 * @return array|false|string[]
 */
function db_lookup_guid($db_cxn, $guid)
{
	$clean_guid = mysqli_escape_string($db_cxn, $guid);
	$sql = "SELECT * FROM member_info WHERE guid = '$clean_guid'";
	$db_res = mysqli_query($db_cxn, "$sql");
	if ( $db_res === false ) {
		write_log("DB ERROR", MY_PID, "GUID Lookup query failure");

		return false;
	}
	$db_list = mysqli_fetch_assoc($db_res);
	if ( $db_list === null ) {
		write_log("INFO", MY_PID, "Invalid GUID submitted");
		write_log("INFO", MY_PID, "Original GUID: {$guid}");
		write_log("INFO", MY_PID, "   Clean GUID: {$clean_guid}");
		$member_info = false;
	} else {
		$member_info = $db_list;
	}
	return $member_info;
}

/**
 * @param $db_cxn
 * @param $conv_id
 * @return array|false|string[]
 */
function db_lookup_conv($db_cxn, $conv_id)
{
	$sql = "SELECT * FROM convention_info WHERE conv_id = '{$conv_id}'";
	$db_res = mysqli_query($db_cxn, "$sql");
	if ( $db_res === false ) {
		write_log("DB ERROR", "db_lookup_conv", "Convention Lookup query failure");
		$conv_info = false;
	} else {
		$db_list = mysqli_fetch_assoc($db_res);
		if ( $db_list === null ) {
			$conv_info = false;
		} else {
			$conv_info = $db_list;
		}
	}
	return $conv_info;
}

/**
 * @param $db_cxn
 * @param $conv_server
 * @return array|false|string[]|null
 */
function db_match_request($db_cxn, $conv_server)
{
	$find_conv_sql = "select *
						from convention_info
						where virt_server = '$conv_server'
						order by conv_id desc
						limit 1";
	$find_conv_result = mysqli_query($db_cxn, "$find_conv_sql");
	if ( $find_conv_result === false ) {
		$return_value = false;
	} else {
		if ( mysqli_num_rows($find_conv_result) == 1 ) {
			$return_value = mysqli_fetch_assoc($find_conv_result);
		} else {
			$return_value = false;
			write_log("ERROR", "db_match_request", "Found multiple rows for {$conv_server}");
		}
	}
	return $return_value;
}

/**
 * @param $db_cxn
 * @param $conv_id
 * @return array|false|null
 */
function db_member_list($db_cxn, $conv_id)
{
	$member_list_sql = "select guid from member_info where conv_id = '{$conv_id}';";
	$member_list_result = mysqli_query($db_cxn, "$member_list_sql");
	if ( $member_list_result === false ) {
		$return = false;
	} else {
		while ( ($member_list_list = mysqli_fetch_assoc($member_list_result)) !== null ) {
			$return[] = $member_list_list['guid'];
		}
	}
	if ( !isset($return) ) {
		$return = null;
	}
	return $return;
}

/**
 * @param $db_cxn
 * @param $conv_id
 * @param $first_name
 * @param $last_name
 * @param $email
 * @return bool|null
 */
function db_member_check($db_cxn, $conv_id, $first_name, $last_name, $email)
{
	if ( DEBUG ) {
		echo "Starting db_member_check(xxxxx, $conv_id, $first_name, $last_name, $email)\n";
	}
	$member_lookup_sql = "select * from member_info where conv_id = '$conv_id' and name_first = '$first_name' and name_last = '$last_name' and email = '$email';";
	if ( VERBOSE ) {
		write_log("VERBOSE", MY_PID, "db_member_check SQL == $member_lookup_sql");
	}
	$member_lookup_result = mysqli_query($db_cxn, "$member_lookup_sql");
	if ( $member_lookup_result === false ) {
		$return_value = false;
	} else {
		while ( ($member_lookup_list = mysqli_fetch_assoc($member_lookup_result)) !== null ) {
			$return_value = true;
		}
	}
	if ( !isset($return_value) ) {
		$return_value = null;
	}
	return $return_value;
}

/**
 * @param $db_cxn
 * @param $conv_id
 * @param $email
 * @return array|false|int
 */
function db_check_email($db_cxn, $conv_id, $email)
{
	$check_email_sql = "select * from member_info where conv_id = '$conv_id' and email = '$email';";
	$check_email_result = mysqli_query($db_cxn, "$check_email_sql");
	if ( $check_email_result === false ) {
		$return_value = false;
	} elseif ( mysqli_num_rows($check_email_result) == 0 ) {
		$return_value = 0;
	} else {
		while ( ($check_email_list = mysqli_fetch_assoc($check_email_result)) ) {
			$return_value[] = $check_email_list;
		}
	}
	return $return_value;
}

/**
 * @param $db_cxn
 * @param $guid
 * @return array|false|null
 */
function db_member_lookup($db_cxn, $guid)
{
	$member_lookup_sql = "select * from member_info where guid = '{$guid}';";
	$member_lookup_result = mysqli_query($db_cxn, "$member_lookup_sql");
	if ( $member_lookup_result === false ) {
		$return = false;
	} else {
		while ( ($member_lookup_list = mysqli_fetch_assoc($member_lookup_result)) !== null ) {
			$return = $member_lookup_list;
		}
	}
	if ( !isset($return) ) {
		$return = null;
	}
	return $return;
}

/**
 * @param $db_cxn
 * @param $guid
 * @param $role
 * @param $grant
 * @return bool|mysqli_result
 */
function db_modify_member_role($db_cxn, $guid, $role, $grant)
{
	// TODO Protect against SQL injection here by authorized users
	$db_columm = "flag_{$role}";

	$modify_member_role_sql = "UPDATE member_info SET {$db_columm} = '{$grant}' WHERE guid = '{$guid}';";
	$modify_member_role_result = mysqli_query($db_cxn, "$modify_member_role_sql");

	return $modify_member_role_result;
}

/**
 * @param $db_cxn
 * @param $conv_id
 * @param $one_time_code
 * @return bool
 *
 * Verify a OTC, make sure that we have it in the one_time_codes table
 */
function check_one_time_code($db_cxn, $conv_id, $one_time_code)
{
	$sql = "SELECT * FROM one_time_codes
		WHERE conv_id = '$conv_id'
		  AND one_time_code = '$one_time_code'";
	$result = mysqli_query($db_cxn, "$sql");
	if ( $result === false ) {
		$return_value = false;
	} else {
		$number_of_rows_found = mysqli_num_rows($result);
		if ( $number_of_rows_found == 1 ) {
			$return_value = true;
		} else {
			$return_value = false;
		}
	}
	return $return_value;
}

/**
 * @param $db_cxn
 * @param $conv_id
 * @param $one_time_code
 * @param $otc_guid
 * @return bool|mysqli_result
 *
 * Update the GUID associated with a $conv_id and $one_time_code
 */
function set_otc_guid($db_cxn, $conv_id, $one_time_code)
{
	$sql = "UPDATE one_time_codes
			WHERE conv_id = '$conv_id'
			  AND one_time_code = '$one_time_code'";
	$result = mysqli_query($db_cxn, $sql);

	return $result;
}

/**
 * @param $db_cxn
 * @param $guid
 * @param $conv_id
 * @param $email
 * @param $name_badge
 * @param $name_first
 * @param $name_last
 * @return bool|mysqli_result
 */
function add_member($db_cxn, $guid, $conv_id, $reg_timestamp, $email, $name_badge, $name_first, $name_last, $prog_flag = false, $c_flag_1 = false, $c_flag_2 = false, $c_flag_3 = false, $c_flag_4 = false)
{
	$prog_flag_val = $prog_flag ? '1' : '0';
	$c_flag_1_val = $c_flag_1 ? '1' : '0';
	$c_flag_2_val = $c_flag_2 ? '1' : '0';
	$c_flag_3_val = $c_flag_3 ? '1' : '0';
	$c_flag_4_val = $c_flag_4 ? '1' : '0';
	$reg_timestamp_unix = strtotime($reg_timestamp);
	$timestamp = date('Y-m-d H:i:s', $reg_timestamp_unix);
	$sql = "INSERT INTO member_info (guid, conv_id, reg_timestamp, email,name_badge, name_first, name_last, flag_admin, flag_program, flag_committee, flag_staff, flag_tech, c_flag_1, c_flag_2, c_flag_3, c_flag_4) VALUES ('$guid','$conv_id','$timestamp','$email','$name_badge','$name_first','$name_last',0,$prog_flag_val,0,0,0,$c_flag_1_val,$c_flag_2_val,$c_flag_3_val,$c_flag_4_val)";
	$result = mysqli_query($db_cxn, "$sql");

	return $result;
}

function update_flag($db_cxn, $guid, $column, $value)
{
	switch ($column) {
		case "c_flag_1":
		case "c_flag_2":
		case "c_flag_3":
		case "c_flag_4":
			$sql = "UPDATE member_info SET $column = $value where guid = '$guid';";
			$result = mysqli_query($db_cxn, $sql);
			break;
		default:
			write_log('ERROR', MY_PID, "Did not update flag $column with value $value as that column is not supported.");
			$result = false;
			break;
	}

	return $result;
}

/**
 * @param $db_cxn
 * @param $conv_id
 * @param $otc
 * @param $otc_guid
 * @return bool|mysqli_result
 */
function remove_one_time_code($db_cxn, $conv_id, $otc)
{
	$sql = "DELETE FROM one_time_codes
				WHERE one_time_code = '$otc'
				  AND conv_id = '$conv_id'";
	$result = mysqli_query($db_cxn, "$sql");

	return $result;
}

/**
 * @param $db_cxn
 * @param $conv_id
 * @param $member_id
 * @param $page_name
 * @return bool|mysqli_result
 */
function log_page_access($db_cxn, $conv_id, $member_id, $page_name)
{

	$sql = "INSERT into page_track (conv_id, member_id, page_name) VALUES ( '$conv_id', '$member_id', '$page_name')";
	$result = mysqli_query($db_cxn, "$sql");

	return $result;
}
