<?php
/**
 * Copyright 2024 Paul Kraus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

/*
 * Handles all incoming requests and provides the user the correct content based on their GUID
 */
ini_set('display_errors', false);

// where do we start
define('SCRIPT_NAME', substr(basename($_SERVER['SCRIPT_FILENAME']), 0, -4));
$script_name = substr(basename($_SERVER['SCRIPT_FILENAME']), 0, -4);
define('SCRIPT_PATH', dirname($_SERVER['SCRIPT_FILENAME']));
$script_path = dirname($_SERVER['SCRIPT_FILENAME']);
define('SCRIPT_PATH_PARENT', dirname($_SERVER['SCRIPT_FILENAME'], 2));
$script_path_parent = dirname($_SERVER['SCRIPT_FILENAME'], 2);

// Default configuration (required)
require_once(SCRIPT_PATH_PARENT . "/virtcon.config");
// Database configuration
// if ( is_file("$db_configuration_path/db_config") ) include_once("$db_configuration_path/db_config");
// if ( is_file("$script_path_parent/db_config") ) include_once("$script_path_parent/db_config");

/*
 * Flags for enhanced logging (VERBOSE), diagnostics (DEBUG), and test mode (TEST)
 */
if ( !defined('DEBUG') ) define('DEBUG', false);
if ( !defined('VERBOSE') ) define('VERBOSE', false);
if ( !defined('TEST') ) define('TEST', false);

/*
 * The following needs to go here and not in a config file because we need the script name
 */
if ( !defined('LOG_DIR') ) define('LOG_DIR', "$log_dir");
ini_set('log_errors', true);
ini_set('error_log', LOG_DIR . "/" . SCRIPT_NAME . "/{$script_name}_php.log");
ini_set('session.use_strict_mode', '1');
define('LOG_FILE', LOG_DIR . "/" . SCRIPT_NAME . ".log");

$html_header_add_ons = "";

// who am i
define('MY_PID', getmypid());


/* includes ******************************************************************/
$inc_path = SCRIPT_PATH_PARENT . "/include";
$inc_files_a = scandir($inc_path);
foreach ($inc_files_a as $inc_file) {
	if ( isset($suffix) ) unset($suffix);
	$suffix = substr("$inc_file", -4, 4);
	if ( "$suffix" == ".php" ) require_once "{$inc_path}/{$inc_file}";
}
/*****************************************************************************/
write_log("INFO", MY_PID, "Starting");

/*
 * Open a connection to the virtualCon database, we can't do anything without that connection
 */
$virtcon_dbx = connect_db(VIRTCON_DB_USER, VIRTCON_DB_PASS, VIRTCON_DB_NAME, VIRTCON_DB_HOST, VIRTCON_DB_PORT);
if ( $virtcon_dbx === false ) {
	write_log("ERROR", MY_PID, "Failed to open VirtCon DB connection");
	//TODO Gracefully exit here
}


/*
 * Start a session, if there is a valid existing session we get that info,
 * otherwise we can see that this browser has not established a session yet
 */
session_start();
if ( isset($_SESSION['conv_id']) ) {
	$conv_id = $_SESSION['conv_id'];
}
/*
 * If we did not have a session,
 * and the user submitted GUID,
 * then try to match the GUID
 */
if ( isset($_REQUEST['auth']) && !isset($conv_id) ) {
	/*
	 * The user submitted a GUID
	 */
	$member_info_list = db_lookup_guid($virtcon_dbx, $_REQUEST['auth']);
	if ( $member_info_list !== false ) {
		$conv_id = $member_info_list['conv_id'];
	}
}
/*
 * If we did not have a session and either the user did not submit a GUID or we did not match it,
 * then lookup the convention by the server URL and assume it is the most recent,
 */
if ( !isset($conv_id) ) {
	$request_server = $_SERVER['SERVER_NAME'];
	$conv_info_list = db_match_request($virtcon_dbx, "$request_server");
	if ( isset($conv_info_list) ) {
		if ( $conv_info_list !== false && $conv_info_list !== null ) {
			$conv_id = $conv_info_list['conv_id'];
		} else {
			unset($conv_info_list);
		}
	}
}
/*
 * Set $conv_info_list if we have a $conv_id and $conv_info_list has not yet been set
 */
if ( isset($conv_id) && !isset($conv_info_list) ) $conv_info_list = db_lookup_conv($virtcon_dbx, $conv_id);

if ( isset($conv_info_list) ) {
	define('CONV_TITLE', $conv_info_list['name_visible']);
	define('CONV_NAME', $conv_info_list['name']);
	define('CONV_YEAR', $conv_info_list['year']);
} else {
	define('CONV_TITLE', "Unknown Convention");
}
$html_header_add_ons .= "<title>" . CONV_TITLE . "</title>";
/*
 * Per convention styles if they exist
 */
if ( defined('CONV_NAME') && defined('CONV_YEAR') ) {
	if ( is_file(SCRIPT_PATH . "/" . CONV_NAME . "/" . CONV_YEAR . "/style.css") ) {
		$conv_css = "/" . CONV_NAME . "/" . CONV_YEAR . "/style.css";
		$html_header_add_ons .= "<link rel='stylesheet' href='{$conv_css}'>";
	} else {
		$html_header_add_ons .= "<link rel='stylesheet' href='/default/style.css'>";
	}
} else {
	$html_header_add_ons .= "<link rel='stylesheet' href='/default/style.css'>";
}
/*
 *
 */
html_header("$html_header_add_ons");
echo "<body>\n";
echo "<div class='virtcon'>";
echo "<div class='top'>";
if ( defined('CONV_NAME') && defined('CONV_YEAR') ) {
	/*
	 * Use the per-convention page header if it exists, otherwise use the default
	 */
	if ( is_file(SCRIPT_PATH_PARENT . "/" . CONV_NAME . "/" . CONV_YEAR . "/page_header.php") ) {
		include SCRIPT_PATH_PARENT . "/" . CONV_NAME . "/" . CONV_YEAR . "/page_header.php";
	} else {
		include SCRIPT_PATH_PARENT . "/default/page_header.php";
	}
}
/*
 * TODO Convention News
 */
if ( !isset($conv_info_list) ) {
	echo "<p>The link you entered is not valid or this convention has not been set up yet. Please double-check the link and try again.</p>";
	echo "</div>"; // close the TOP div
} else {
	echo "</div>"; // close the TOP div
	/*
	 * Check to see if this session is expired
	 */
	if ( isset($_SESSION['expiration']) ) {
		if ( $_SESSION['expiration'] < date('U') ) {
			clear_session();
			echo "<div class='warn'>";
			echo "<p>Your authorization for this convention has expired.</p>\n";
			echo "<p>You need to use the convention link you were sent to re-authorize.</p>\n";
			echo "<p>If you do not have it, you can request a reminder email below.</p>\n";
			echo "</div>\n";
		}
	}

	/*
	 * Set the $member_guid if either the user submitted it or we have it from a session
	 */
	if ( isset($_REQUEST['auth']) ) {
		/*
		 * Make sure the length of the auth request submitted is the correct length
		 */
		if ( strlen($_REQUEST['auth']) == 36 ) {
			$member_guid = $_REQUEST['auth'];
		}
	} else {
		if ( isset($_SESSION['guid']) ) {
			$member_guid = $_SESSION['guid'];
		}
	}
	/*
	 * If we do not have a $member_guid then the user is not registered or submitted a bad guid,
	 * but if they submitted an email address, then just send them a reminder link
	 */
	if ( !isset($member_guid) ) {
		/*
		 * If the request includes an email address, send a link reminder email to that address
		 */
		if ( isset($_REQUEST['email']) ) {
			write_log("INFO", MY_PID, "Start of email reminder");
			$email_reminder = $_REQUEST['email'];
			/*
			 * The email reminder has 2 phases, the first captures the user's email address and
			 * the second is a confirmation (designed to prevent malicious email requests)
			 *
			 * The second phase should have an email value of 'blarg' and an active session,
			 * if it does not, then it is malformed or malicious, so make them start over.
			 *
			 * If the email address is not 'blarg' (an obviously fake email), then
			 * we assume this is phase 1.
			 */
			if ( $email_reminder == 'blarg' && !isset($_SESSION['email_reminder']) ) {
				/*
				 * This is malformed or bookmarked or attempted hack
				 */
				echo "<p>Invalid request received. Please start over <a href='/'>here.</a></p>\n";
			} else {
				send_email_reminder($email_reminder);
			}
		} else {
			/*
			 * Not authenticated, show them the reg / reminder page
			 */
			not_registered();
		}
	} else {
		$member_info_list = db_lookup_guid($virtcon_dbx, $member_guid);
		if ( $member_info_list === false ) {
			not_registered();
		} else {
			/*
			 * ******************************
			 * This is the Virtual Convention
			 * ******************************
			 */
			/*
			 * We start by setting the user's $_SESSION if it is not already setup
			 */
			$convention_info_list = db_lookup_conv($virtcon_dbx, $member_info_list['conv_id']);
			$conv_id = $convention_info_list['conv_id'];
			setup_session($member_info_list, $convention_info_list);

			/*
			 * Left Navigation (per convention)
			 */
			echo "<div class='left_nav'>";
			if ( defined('CONV_NAME') && defined('CONV_YEAR') ) {
				if ( is_file(SCRIPT_PATH_PARENT . "/" . CONV_NAME . "/" . CONV_YEAR . "/menu.php") ) {
					include_once SCRIPT_PATH_PARENT . "/" . CONV_NAME . "/" . CONV_YEAR . "/menu.php";
				} elseif ( is_file(SCRIPT_PATH . "/" . CONV_NAME . "/" . CONV_YEAR . "/menu.php") ) {
					include_once SCRIPT_PATH . "/" . CONV_NAME . "/" . CONV_YEAR . "/menu.php";
				} else {
					include_once(SCRIPT_PATH_PARENT . "/default/menu.php");
				}
			} else {
				write_log("ERROR", MY_PID, "Got to Left Nav with no CONV_NAME and CONV_YEAR, this should not occur.");
			}
			echo "</div>\n"; // Cloe the left_nav div

			/*
			 * Main
			 */
			echo "<div class='main'>";
			/*
			 * There is a need to let each convention customize their home page
			 */
			if ( defined('CONV_NAME') && defined('CONV_YEAR') ) {
				if ( is_file(SCRIPT_PATH_PARENT . "/" . CONV_NAME . "/" . CONV_YEAR . "/main.php") ) {
					include_once SCRIPT_PATH_PARENT . "/" . CONV_NAME . "/" . CONV_YEAR . "/main.php";
				} elseif ( is_file(SCRIPT_PATH . "/" . CONV_NAME . "/" . CONV_YEAR . "/main.php") ) {
					include_once SCRIPT_PATH . "/" . CONV_NAME . "/" . CONV_YEAR . "/main.php";
				} else {
					include_once(SCRIPT_PATH . "/default/main.php");
				}
			} else {
				write_log("ERROR", MY_PID, "Got to Left Nav with no CONV_NAME and CONV_YEAR, this should not occur.");
			}

			/*
			if ( isset($_REQUEST['page']) ) {
				switch ($_REQUEST['page']) {
					case "memberlist":
						log_page_access($virtcon_dbx, $conv_id, $member_info_list['name_badge'], "memberlist");
						member_list($conv_id);
						break;
					case "schedule":
						echo "<iframe class='content' src='https://{$convention_info_list['virt_server']}/{$conv_name}/{$conv_year}/konopas/index.php' title='{$convention_info_list['name_visible']}'></iframe>";
						break;
					case "film":
						log_page_access($virtcon_dbx, $conv_id, $member_info_list['name_badge'], "film");
						film_page($conv_id);
						break;
					case "usage":
						log_page_access($virtcon_dbx, $conv_id, $member_info_list['name_badge'], "usage");
						usage_reports($conv_id);
						break;
					case "email":
						log_page_access($virtcon_dbx, $conv_id, $member_info_list['name_badge'], "email");
						manage_email($conv_id);
						break;
					default:
//						log_page_access($virtcon_dbx, $conv_id, $member_info_list['name_badge'], "home");
//						convention_home($convention_info_list);
						log_page_access($virtcon_dbx, $conv_id, $member_info_list['name_badge'], "schedule");
						echo "<iframe class='content' src='https://$convention_info_list[virt_server]/" . CONV_NAME . "/" . CONV_YEAR . "$convention_info_list[schedule_path]' title='{$convention_info_list['name_visible']}'></iframe>";
						break;
				}
			} else {
//				log_page_access($virtcon_dbx, $conv_id, $member_info_list['name_badge'], "home");
//				convention_home($convention_info_list);
				log_page_access($virtcon_dbx, $conv_id, $member_info_list['name_badge'], "schedule");
				echo "<iframe class='content' src='https://$convention_info_list[virt_server]/" . CONV_NAME . "/" . CONV_YEAR . "$convention_info_list[schedule_path]' title='{$convention_info_list['name_visible']}'></iframe>";
			}
			*/
			echo "</div>\n"; // Close the MAIN div
			/*
			 * Footer
			 */
//		convention_footer($member_guid);
		}
	}
}

close_out();
