<?php
/*
 * This is the authorized Konopas base for the convention
 * So if the user does not have a valid $_SESSION for this convention then send them back to the start,
 * otherwise give them the Konopas content.
 */
ini_set('display_errors', false);

define('SCRIPT_PATH', dirname($_SERVER['SCRIPT_FILENAME']));
define('SCRIPT_NAME', substr(basename($_SERVER['SCRIPT_FILENAME']), 0, -4));
define('SCRIPT_PATH_ROOT', dirname($_SERVER['SCRIPT_FILENAME'], 4));

require_once(SCRIPT_PATH_ROOT . "/db_config");
define('LOG_DIR', SCRIPT_PATH_ROOT . "/logs");

// Default configuration (required)
// require_once(SCRIPT_PATH_ROOT . "/virtcon.config");

/*
 * The following needs to go here and not in a config file because we need the script name
 */
ini_set('log_errors', true);
ini_set('error_log', LOG_DIR . "/" . SCRIPT_NAME . "_php.log");
ini_set('session.use_strict_mode', '1');
define('LOG_FILE', LOG_DIR . "/" . SCRIPT_NAME . ".log");

// who am i
define('MY_PID', getmypid());

/* includes ******************************************************************/
$inc_path = SCRIPT_PATH_ROOT . "/include";
$inc_files_a = scandir($inc_path);
foreach ($inc_files_a as $inc_file) {
	if ( isset($suffix) ) {
		unset($suffix);
	}
	$suffix = substr("$inc_file", -4, 4);
	if ( "$suffix" == ".php" ) {
		require_once "{$inc_path}/{$inc_file}";
	}
}
/*****************************************************************************/
write_log("INFO", MY_PID, SCRIPT_NAME . " starting");

session_start();

/*
 * If the session expiration is not set we redirect home
 */
if ( !isset($_SESSION['expiration']) ) {
	redirect_home();
} else {
	/*
	 * If it is past the session expiration we redirect home
	 */
	if ( $_SESSION['expiration'] < now() ) {
		redirect_home();
	}
}
/*
 * If we made it this far we must have a $_SESSION with a current expiration, so just hand it over to KonOpas
 */

write_log("KONOPAS", MY_PID, "Starting KonOpas");
?>
    <!DOCTYPE html>
    <html><!-- manifest="konopas.appcache" -->

    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="skin/konopas.css">
        <meta name="HandheldFriendly" content="true">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimal-ui">

        <!-- Android + favicon -->
        <meta name="mobile-web-app-capable" content="yes">
        <link rel="shortcut icon" sizes="196x196" href="skin/favicon.196.png">

        <!-- iOS -->
        <meta name="apple-mobile-web-app-capable" content="yes">
        <!--
			<link rel="apple-touch-icon"                 href="data/icon.apple.60.png">
			<link rel="apple-touch-icon" sizes="76x76"   href="data/icon.apple.76.png">
			<link rel="apple-touch-icon" sizes="120x120" href="data/icon.apple.120.png">
			<link rel="apple-touch-icon" sizes="152x152" href="data/icon.apple.152.png">
			<link rel="apple-touch-startup-image" href="data/start.apple.1536x2008.png" media="(device-width: 1536px) and (orientation: portrait) and (-webkit-device-pixel-ratio: 2)">
			<link rel="apple-touch-startup-image" href="data/start.apple.2048x1496.png" media="(device-width: 1536px) and (orientation: landscape) and (-webkit-device-pixel-ratio: 2)">
			<link rel="apple-touch-startup-image" href="data/start.apple.768x1004.png"  media="(device-width: 768px) and (orientation: portrait)">
			<link rel="apple-touch-startup-image" href="data/start.apple.1024x748.png"  media="(device-width: 768px) and (orientation: landscape)">
			<link rel="apple-touch-startup-image" href="data/start.apple.640x920.png"   media="(device-width: 320px) and (-webkit-device-pixel-ratio: 2)">
			<link rel="apple-touch-startup-image" href="data/start.apple.320x460.png"   media="(device-width: 320px)">
			-->

        <!-- Windows Phone 8.1 -->
        <!--
			<meta name="msapplication-TileColor"         content="#ffffff">
			<meta name="msapplication-square70x70logo"   content="data/icon.ie.png">
			<meta name="msapplication-square150x150logo" content="data/icon.ie.png">
			<meta name="msapplication-square310x310logo" content="data/icon.ie.png">
			-->

        <title>Smofcon 40</title>
    </head>

    <body>
    <div id="top"></div>
    <div id="load_disable"></div>

    <div id="time"></div>
    <div id="scroll"><a href="#top" id="scroll_link" data-txt>Scroll up</a></div>

    <div id="banner">
        <div id="server_connect"></div>

        <h1>Smofcon 40</h1>

        <ul id="tabs">
            <li id="tab_star"><a href="#star" data-txt>My con</a>
            <li id="tab_prog"><a href="#" data-txt>Program</a>
                <div id="day-sidebar" class="sub-side"></div>
            <li id="tab_part"><a href="#part" data-txt>People</a>
                <div id="part-sidebar" class="sub-side"></div>
            <li id="tab_info"><a href="#info" data-txt>Info</a>
        </ul>

    </div>
    <div id="main">

        <div id="star_view" class="view">
            <div id="star_data"></div>
        </div>

        <div id="prog_view" class="view">
            <div id="prog_filters" class="filters">
                <div id="prog_lists"></div>
                <form name="search" id="search">
                    <input type="text" id="q" required data-txt="Search" data-txt-attr="placeholder">
                    <div id="q_ctrl">
                        <input type="submit" id="q_submit" data-txt="Go" data-txt-attr="value">
                    </div>
                    <div id="q_hint" class="hint"></div>
                    <div id="filter_sum_wrap">
                        <input type="reset" id="q_clear" data-txt="Clear all" data-txt-attr="value">
                        <div id="filter_sum"></div>
                    </div>
                </form>
            </div>
            <p>Times are displayed in the same timezone your device is set to.</p>
            <p>Recordings will be available through at least December 31, 2024.</p>
            <div id="day-narrow" class="sub-narrow"></div>
        </div>

        <div id="part_view" class="view">
            <div id="part-narrow" class="sub-narrow"></div>
            <ul id="part_names"></ul>
            <div id="part_info"></div>
        </div>

        <div id="info_view" class="view">
            <h2>Wayfinding (Map Below)</h2>
            <h3>Lincoln Hospitality and Day Registration</h3>
            <p>When facing the Hotel Front Desk go left (away from the Hotel Restaurant) until you get to the Lincoln
                Room.</p>
            <h3>Upstairs Hospitality and Evening Registration</h3>
            <p>When facing the Hotel Front Desk go left (away from the Hotel Restaurant) until you get to the Guest
                Elevators and take them to the 5th floor. Look for rooms 559 and 560.</p>
            <h3>Program Rooms (Ocean Ballroom)</h3>
            <p>When facing the Hotel Front Desk take the hallway to the right of the Front Desk (the side with the Hotel
                Restaurant) until you get to the Elevators and Stairs. Go down one floor and the Ocean Ballroom will be
                diagonally on the right.</p>
            <p><strong>Please remember that medical grade masks are required in Program Space. We will have some masks
                    available.</strong></p>

            <h3>Map</h3>
            <div>
                <img src="/smofcon40/2023/map.png" title="map of the hotel. see above for written directions."/>
            </div>
            <p>This is the program guide for <a href="https://smofcon40.org/" target="_blank">Smofcon 40</a>. It should
                be
                suitable for use on most browsers and devices. It's an instance of <a href="http://konopas.github.io/"
                                                                                      target="_blank">KonOpas</a>,
                an open-source project providing conventions with easy-to-use mobile-friendly guides.

            <p>
            <div id="last-updated">Program and participant data are updated every 5 minutes.</div>

            <div id="install-instructions">
                <p>This guide will work in your browser even when you don't have an internet connection.
                <h4 class="collapse">You can also install it as a home screen app</h4>
                <dl class="compact-dl">
                    <dt>Safari on iPhone/iPad
                    <dd>Tap on <i class="ios_share_icon"></i> (share), then tap <b>Add to Home Screen</b>.
                    <dt>Chrome on Android
                    <dd>Tap on <i class="android_menu_icon"></i> (menu), then tap <b>Add to Home Screen</b>.
                    <dt>Firefox on Android
                    <dd>Tap on <i class="android_menu_icon"></i> (menu), then tap <b>Bookmark</b> and select
                        <b>Options</b>,
                        and <b>Add to Home Screen</b>.
                    <dt>IE on Windows Phone
                    <dd>Tap on <i class="ie_menu_icon"></i> (menu), then tap <b>Pin to Start</b>
                </dl>
            </div>
            <script>
                if (navigator.standalone) document.getElementById('install-instructions').style.display = 'none';
            </script>

        </div>


        <div id="prog_ls" class="ls"><br>
            <div class="ls_loading">Loading program data&hellip;</div>
        </div>

    </div><!-- /main -->

    </body>
    <script src="konopasDataLinks.jsonp"></script>
    <script src="/smofcon40/2023/konopas2.min.js"></script>
    <script>
        var konopas = new KonOpas({
            'id': 'smofcon-40',
            'default_duration': 60,
            'time_show_am_pm': true,
            'show_all_days_by_default': true,
            'use_server': false
        });
        konopas.set_view();
        program = program.map(i => {
            var d = new Date(i.datetime);
            i.date = "" + d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate()
            i.time = d.toTimeString().substr(0, 5);
            delete i.datetime;
            i.tags = [];
            return i;
        })
        konopas.set_program(program, {
            'day': {},
            'area': {},
            'tag': {}
        });
        people = people.map(p => {
            p.name = [p.name]
            return p;
        })
        konopas.set_people(people);
        konopas.set_view('prog');
        // fetch("https://planorama.smofcon40.org/conclar/schedule?links=true")
        //     .then(resp => {
        //         resp.json().then(program => {
        //             console.log('program', program)
        //             program = program.map(i => {
        //                 var d = new Date(i.datetime);
        //                 i.date = "" + d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate()
        //                 i.time = d.toTimeString().substr(0, 5);
        //                 delete i.datetime;
        //                 i.tags = [];
        //                 return i;
        //             })
        //             konopas.set_program(program, {
        //                 'day': {},
        //                 'area': {},
        //                 'tag': {}
        //             });
        //             konopas.set_view();
        //         })
        //     })
        // fetch('https://planorama.smofcon40.org/conclar/participants')
        //     .then(resp => {
        //         resp.json().then(people => {
        //             console.log("people", people)
        //             people = people.map(p => {
        //                 p.name = [p.name]
        //                 return p;
        //             })
        //             konopas.set_people(people);
        //         })
        //     })
    </script>

<?php
write_log("KONOPAS", MY_PID, "Got to the bottom of the KonOpas code");
