<?php
echo "<nav>";
// TODO Make menu scalable and not hard coded
echo "<ul>";
echo "<li><a href='https://smofcon40.org' target='blank'>Convention Website</a></li>";
echo "<li><a href='index.php?page=schedule'>Schedule</a></li>";
if ( $_SESSION['flag_admin'] == 1 || $_SESSION['flag_committee'] == 1 || $_SESSION['flag_staff'] == 1 || $_SESSION['flag_tech'] == 1 || $_SESSION['flag_program'] == 1 ) {
	/*
	 * Menu items that anyone who has more roles that just member
	 */

}
if ( $_SESSION['flag_admin'] == 1 || $_SESSION['flag_committee'] == 1 ) {
	/*
	 * Menu items that anyone who has admin or committee
	 */
}
if ( $_SESSION['flag_admin'] == 1 ) {
	/*
	 * Menu items just for Admin role
	 */
	echo "<li><a href='index.php?page=memberlist'>Membership List</a></li>";
	echo "<li><a href='index.php?page=email'>Send Email</a></li>";
}
echo "</ul>";
echo "</nav>";

// TODO let member edit their Badge Name and Email
echo "<div class=\"user-info\">";
echo "<span>You are: {$_SESSION['name_first']} {$_SESSION['name_last']} </span>";
echo "<span>{$_SESSION['email']}</span>";
if ( $_SESSION['name_badge'] != "" ) {
	echo "<span>Badge name: {$_SESSION['name_badge']}</span>";
}
echo "</div>";
