<?php
/*
 * Custom main.php for smofcon40
 */
if ( isset($_REQUEST['page']) ) {
	switch ($_REQUEST['page']) {
		case "memberlist":
			log_page_access($virtcon_dbx, $conv_id, $member_info_list['name_badge'], "memberlist");
			member_list($conv_id);
			break;
		case "schedule":
			log_page_access($virtcon_dbx, $conv_id, $member_info_list['name_badge'], "schedule");
			echo "<iframe class='content' src='https://$convention_info_list[virt_server]/" . CONV_NAME . "/" . CONV_YEAR . "$convention_info_list[schedule_path]' title='{$convention_info_list['name_visible']}'></iframe>";
			break;
		case "film":
			log_page_access($virtcon_dbx, $conv_id, $member_info_list['name_badge'], "film");
			film_page($conv_id);
			break;
		case "usage":
			log_page_access($virtcon_dbx, $conv_id, $member_info_list['name_badge'], "usage");
			usage_reports($conv_id);
			break;
		case "email":
			log_page_access($virtcon_dbx, $conv_id, $member_info_list['name_badge'], "email");
			manage_email($conv_id);
			break;
		default:
			log_page_access($virtcon_dbx, $conv_id, $member_info_list['name_badge'], "schedule");
			echo "<iframe class='content' src='https://$convention_info_list[virt_server]/" . CONV_NAME . "/" . CONV_YEAR . "$convention_info_list[schedule_path]' title='{$convention_info_list['name_visible']}'></iframe>";
			break;
	}
} else {
	log_page_access($virtcon_dbx, $conv_id, $member_info_list['name_badge'], "schedule");
	echo "<iframe class='content' src='https://$convention_info_list[virt_server]/" . CONV_NAME . "/" . CONV_YEAR . "$convention_info_list[schedule_path]' title='{$convention_info_list['name_visible']}'></iframe>";
}
