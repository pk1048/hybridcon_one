<?php
/**
 * Copyright 2024 Paul Kraus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

/*
 * This is the authorized Konopas base for the convention
 * So if the user does not have a valid $_SESSION for this convention then send them back to the start,
 * otherwise give them the Konopas content.
 */
ini_set('display_errors', true);

// where do we start
$script_path = dirname($_SERVER['SCRIPT_FILENAME']);
$script_name = substr(basename($_SERVER['SCRIPT_FILENAME']), 0, -4);
$script_path_parent = dirname($_SERVER['SCRIPT_FILENAME'], 2);

// Default configuration (required)
require_once("{$script_path_parent}/virtcon.config");
// Optional configuration (overrides the default above)
if ( is_file("{$script_path_parent}/{$script_name}.config") ) {
	include_once("{$script_path_parent}/{$script_name}.config");
}


/*
 * The following needs to go here and not in a config file because we need the script name
 */
ini_set('log_errors', true);
ini_set('error_log', "{$log_dir}/{$script_name}_php.log");
ini_set('session.use_strict_mode', '1');
$log_file = "{$log_dir}/{$script_name}.log";

// who am i
$mypid = getmypid();

/* includes ******************************************************************/
$inc_path = "{$script_path_parent}/include";
$inc_files_a = scandir($inc_path);
foreach ($inc_files_a as $inc_file) {
	if ( isset($suffix) ) {
		unset($suffix);
	}
	$suffix = substr("$inc_file", -4, 4);
	if ( "$suffix" == ".php" ) {
		require_once "{$inc_path}/{$inc_file}";
	}
}
/*****************************************************************************/
write_log("INFO", "$mypid", "{$script_name} starting");

session_start();

/*
 * If the session expiration is not set we redirect home
 */
if ( !isset($_SESSION['expiration']) ) {
	redirect_home();
} else {
	/*
	 * If it is past the session expiration we redirect home
	 */
	if ( $_SESSION['expiration'] < now() ) {
		redirect_home();
	}
}
/*
 * If we made it this far we must have a $_SESSION with a current expiration, so just hand it over to KonOpas
 */

write_log("KONOPAS", "$mypid", "Starting KonOpas");
?>
    <!DOCTYPE html>
    <html><!-- manifest="konopas.appcache" -->
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="skin/konopas.css">
        <meta name="HandheldFriendly" content="true">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimal-ui">

        <!-- Android + favicon -->
        <meta name="mobile-web-app-capable" content="yes">
        <link rel="shortcut icon" sizes="196x196" href="skin/favicon.196.png">

        <!-- iOS -->
        <meta name="apple-mobile-web-app-capable" content="yes">
        <!--
		<link rel="apple-touch-icon"                 href="data/icon.apple.60.png">
		<link rel="apple-touch-icon" sizes="76x76"   href="data/icon.apple.76.png">
		<link rel="apple-touch-icon" sizes="120x120" href="data/icon.apple.120.png">
		<link rel="apple-touch-icon" sizes="152x152" href="data/icon.apple.152.png">
		<link rel="apple-touch-startup-image" href="data/start.apple.1536x2008.png" media="(device-width: 1536px) and (orientation: portrait) and (-webkit-device-pixel-ratio: 2)">
		<link rel="apple-touch-startup-image" href="data/start.apple.2048x1496.png" media="(device-width: 1536px) and (orientation: landscape) and (-webkit-device-pixel-ratio: 2)">
		<link rel="apple-touch-startup-image" href="data/start.apple.768x1004.png"  media="(device-width: 768px) and (orientation: portrait)">
		<link rel="apple-touch-startup-image" href="data/start.apple.1024x748.png"  media="(device-width: 768px) and (orientation: landscape)">
		<link rel="apple-touch-startup-image" href="data/start.apple.640x920.png"   media="(device-width: 320px) and (-webkit-device-pixel-ratio: 2)">
		<link rel="apple-touch-startup-image" href="data/start.apple.320x460.png"   media="(device-width: 320px)">
		-->

        <!-- Windows Phone 8.1 -->
        <!--
		<meta name="msapplication-TileColor"         content="#ffffff">
		<meta name="msapplication-square70x70logo"   content="data/icon.ie.png">
		<meta name="msapplication-square150x150logo" content="data/icon.ie.png">
		<meta name="msapplication-square310x310logo" content="data/icon.ie.png">
		-->

        <title>Balticon 56</title>

    <body>
    <div id="top"></div>
    <div id="load_disable"></div>

    <div id="time"></div>
    <div id="scroll"><a href="#top" id="scroll_link" data-txt>Scroll up</a></div>

    <div id="banner">
        <div id="server_connect"></div>

        <h1><a href="#info" alt="Balticon 56" title="Balticon 56">
                <img id="title-small" src="https://schedule.balticon.org/data/cropped-balticon_favicon_dark.png"
                     alt="Balticon logo" style="width: 240px;">
                <img id="title" src="https://schedule.balticon.org/data/cropped-balticon_favicon_dark.png"
                     alt="BSFS logo"
                     style="width: 200px; margin: auto;">

                <ul id="tabs">
                    <li id="tab_star"><a href="#star" data-txt>My con</a>
                    <li id="tab_prog"><a href="#" data-txt>Program</a>
                        <div id="day-sidebar" class="sub-side"></div>
                    <li id="tab_part"><a href="#part" data-txt>People</a>
                        <div id="part-sidebar" class="sub-side"></div>
                        <!--            <li id="tab_info"><a href="#info" data-txt>Info</a> -->
                </ul>

    </div>
    <div id="main">

        <div id="star_view" class="view">
            <div id="star_data"></div>
        </div>

        <div id="prog_view" class="view">
            <div id="prog_filters" class="filters">
                <div id="prog_lists"></div>
                <form name="search" id="search">
                    <input type="text" id="q" required data-txt="Search" data-txt-attr="placeholder">
                    <div id="q_ctrl">
                        <input type="submit" id="q_submit" data-txt="Go" data-txt-attr="value">
                    </div>
                    <div id="q_hint" class="hint"></div>
                    <div id="filter_sum_wrap">
                        <input type="reset" id="q_clear" data-txt="Clear all" data-txt-attr="value">
                        <div id="filter_sum"></div>
                    </div>
                </form>
            </div>
            <div id="day-narrow" class="sub-narrow"></div>
        </div>

        <div id="part_view" class="view">
            <div id="part-narrow" class="sub-narrow"></div>
            <ul id="part_names"></ul>
            <div id="part_info"></div>
        </div>

        <div id="info_view" class="view">
            <p>This is the program guide for <a href="https://albacon.org/">Virtual Albacon 2021</a>. It should be
                suitable for use on most browsers and devices. It's an instance of <a
                        href="http://konopas.org/">KonOpas</a>,
                an open-source project providing conventions with easy-to-use mobile-friendly guides.

            <p>
            <div id="last-updated">Program and participant data are updated every 10 minutes.</div>

            <div id="install-instructions">
                <p>This guide will work in your browser even when you don't have an internet connection.
                <h4 class="collapse">You can also install it as a home screen app</h4>
                <dl class="compact-dl">
                    <dt>Safari on iPhone/iPad
                    <dd>Tap on <i class="ios_share_icon"></i> (share), then tap <b>Add to Home Screen</b>.
                    <dt>Chrome on Android
                    <dd>Tap on <i class="android_menu_icon"></i> (menu), then tap <b>Add to Home Screen</b>.
                    <dt>Firefox on Android
                    <dd>Tap on <i class="android_menu_icon"></i> (menu), then tap <b>Bookmark</b> and select
                        <b>Options</b>,
                        and <b>Add to Home Screen</b>.
                    <dt>IE on Windows Phone
                    <dd>Tap on <i class="ie_menu_icon"></i> (menu), then tap <b>Pin to Start</b>
                </dl>
            </div>
            <script>
                if (navigator.standalone) document.getElementById('install-instructions').style.display = 'none';
            </script>

            <!-- SAMPLE SECTIONS FOR "INFO" VIEW, UNCOMMENT TO SEE
			<style>
			#map { width: 100% !important; }
			#map td { text-align: center !important; padding: 6px 32px; }
			#map a, #map span { font-family: 'Oswald'; font-weight: 400; font-size: 1.2em; cursor: pointer; }
			#map a:hover, #map span:hover { text-decoration: underline; }
			#map tr:hover, .quick-ref tr:hover { background: inherit !important; }
			.quick-ref dt { margin: 6px 0 3px; }
			.quick-ref table { width: auto !important; border-spacing: 0; margin-bottom: 6px; }
			.quick-ref td { text-align: left !important; }
			#info_view .quick-ref td.c { text-align: center !important; }
			.quick-ref td:nth-child(2) { text-align: right !important; padding: 0 0 0 5px; }
			.quick-ref td:nth-child(3) { padding-left: 0; }
			</style>

			<h2>Sample Maps</h2>
			<table id="map">
			<tr><td><a class="popup-link" href="http://guide.2014.arisia.org/data/arisia-westin-3W.png">Mezzanine Level&nbsp;(3W)</a>
				<td><a class="popup-link" href="http://guide.2014.arisia.org/data/arisia-westin-3E.png">Conference Level&nbsp;(3E)</a>
			<tr><td colspan="2"><a class="popup-link" href="http://guide.2014.arisia.org/data/arisia-westin-2.png">Lobby Level&nbsp;(2)</a>
			<tr><td><a class="popup-link" href="http://guide.2014.arisia.org/data/arisia-westin-1W.png">Concourse Level&nbsp;(1W)</a>
				<td><a class="popup-link" href="http://guide.2014.arisia.org/data/arisia-westin-1E.png">Galleria Level&nbsp;(1E)</a>
			</table>

			<h2>Sample Links</h2>
			<ul>
			<li><a href="http://example.com/">Newsletter</a>
			<li><a href="http://example.com/">Restaurant Guide</a> (PDF, 6MB)
			<li><a href="http://example.com/">Progress Report 5</a> (web page)
			<li><a href="http://example.com/">Code of Conduct</a> (PDF)
			</ul>

			<h2>Useful Information</h2>
			<dl class="quick-ref">
				<dt><b>Information Desk</b>: stop in Town Hall</dt>
				<dt><b>Ice Cream Social</b>: Town Hall</dt>
				<dt><b>Program Items</b>: in individual Zoom meetings</dt>
				<dt><b>Readings</b>: in Breakout rooms off of Town Hall</dt>

				<dd>
					<table>
						<tr>
							<td>Friday
							<td>8pm
							<td>10pm
					</table>

				<dt><b>...</b>
			</dl>
						-->

        </div>


        <div id="prog_ls" class="ls"><br>
            <div class="ls_loading">Loading program data&hellip;</div>
        </div>

    </div><!-- /main -->

    </body>
    <script src="https://zambia.balticon.org/konOpasSecureData.jsonp"></script>
    <script src="konopas2.min.js"></script>
    <script>
        var konopas = new KonOpas({
            'id': 'balticon-56',
            'default_duration': 0,
            'time_show_am_pm': true,
            'show_all_days_by_default': true,
            'use_server': false
        });
        konopas.set_program(program, {
            'day': {},
            'area': {},
            'tag': {}
        });
        konopas.set_people(people);
        konopas.set_view();
    </script>

<?php
write_log("KONOPAS", "$mypid", "Got to the bottom of the KonOpas code");
