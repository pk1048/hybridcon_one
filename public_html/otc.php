<?php
/**
 * Copyright 2024 Paul Kraus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

ini_set('display_errors', true);

// where do we start
define('SCRIPT_PATH', dirname($_SERVER['SCRIPT_FILENAME']));
// $script_path = dirname($_SERVER['SCRIPT_FILENAME']);
define('SCRIPT_NAME', substr(basename($_SERVER['SCRIPT_FILENAME']), 0, -4));
// $script_name = substr(basename($_SERVER['SCRIPT_FILENAME']), 0, -4);
define('SCRIPT_PATH_PARENT', dirname($_SERVER['SCRIPT_FILENAME'], 2));
// $script_path_parent = dirname($_SERVER['SCRIPT_FILENAME'], 2);

// Default configuration (required)
require_once(SCRIPT_PATH_PARENT . "/virtcon.config");
// Database configuration
// if ( is_file("$db_configuration_path/db_config") ) include_once("$db_configuration_path/db_config");
// if ( is_file("$script_path_parent/db_config") ) include_once("$script_path_parent/db_config");

/*
 * The following needs to go here and not in a config file because we need the script name
 */
// $log_dir is defined in virtcon.config
if ( !defined('LOG_DIR') ) define('LOG_DIR', "$log_dir");
ini_set('log_errors', true);
ini_set('error_log', LOG_DIR . "/" . SCRIPT_NAME . "/" . SCRIPT_NAME . "_php.log");
ini_set('session.use_strict_mode', '1');
define('LOG_FILE', LOG_DIR . "/" . SCRIPT_NAME . ".log");

if ( !defined('DEBUG') ) define('DEBUG', false);
if ( !defined('VERBOSE') ) define('VERBOSE', false);

// who am I
define('MY_PID', getmypid());

$html_header_add_ons = "";

/* includes ******************************************************************/
$inc_path = SCRIPT_PATH_PARENT . "/include";
$inc_files_a = scandir($inc_path);
foreach ($inc_files_a as $inc_file) {
	if ( isset($suffix) ) unset($suffix);
	$suffix = substr("$inc_file", -4, 4);
	if ( "$suffix" == ".php" ) require_once "{$inc_path}/{$inc_file}";
}
/*****************************************************************************/
write_log("INFO", MY_PID, "Starting");
$conv_server = $_SERVER['SERVER_NAME'];
$conv_url = "https://{$conv_server}/" . SCRIPT_NAME . ".php";
/*
 * Set the error flag to false, any missing data will set it to true
 */
$error_flag = false;
/*
 * Open a connection to the virtualCon database, we can't do anything without that connection
 */
$dbx_error_flag = false;
$virtcon_dbx = connect_db(VIRTCON_DB_USER, VIRTCON_DB_PASS, VIRTCON_DB_NAME, VIRTCON_DB_HOST, VIRTCON_DB_PORT);
if ( $virtcon_dbx === false ) {
	write_log("ERROR", MY_PID, "Failed top open DB connection");
	$dbx_error_flag = true;
}
/*
 * Parse the submitted fields
 */
if ( isset($_REQUEST['first_name']) ) {
	$first_name = mysqli_real_escape_string($virtcon_dbx, $_REQUEST['first_name']);
} else {
	write_log("OTC_WARN", MY_PID, "Failed to find first_name");
	$error_flag = true;
}
if ( isset($_REQUEST['last_name']) ) {
	$last_name = mysqli_real_escape_string($virtcon_dbx, $_REQUEST['last_name']);
} else {
	write_log("OTC_WARN", MY_PID, "Failed to find last_name");
	$error_flag = true;
}
if ( isset($_REQUEST['badge_name']) ) {
	$badge_name = mysqli_real_escape_string($virtcon_dbx, $_REQUEST['badge_name']);
} else {
	write_log("OTC_WARN", MY_PID, "Failed to find badge_name");
	$error_flag = true;
}
if ( isset($_REQUEST['email']) ) {
	$email = mysqli_real_escape_string($virtcon_dbx, $_REQUEST['email']);
} else {
	write_log("OTC_WARN", MY_PID, "Failed to find email");
	$error_flag = true;
}
if ( isset($_REQUEST['conv_id']) ) {
	$conv_id = mysqli_real_escape_string($virtcon_dbx, $_REQUEST['conv_id']);
} else {
	write_log("OTC_WARN", MY_PID, "Failed to find conv_id");
	$error_flag = true;
}
if ( isset($_REQUEST['otc']) ) {
	$one_time_code = mysqli_real_escape_string($virtcon_dbx, $_REQUEST['otc']);
} else {
	write_log("OTC_WARN", MY_PID, "Failed to find one_time_code");
	$error_flag = true;
}
if ( isset($_REQUEST['guid']) ) {
	$otc_guid = mysqli_real_escape_string($virtcon_dbx, $_REQUEST['guid']);
} else {
	write_log("OTC_WARN", MY_PID, "Failed to find otc_guid");
	$error_flag = true;
}
/*
 * Start building the web page, we will add content as appropriate below.
 */
echo "<!DOCTYPE html>\n";
echo "<html lang='en>\n";
echo "<head>\n";
echo "<meta charset=' utf-8'>\n";
echo "<link rel='stylesheet' href='default/style.css'>\n";
/*
 * If we have all the required values, then the $error_flag is set to false AND we have a $virtcon_dbx
 */
if ( $dbx_error_flag === true ) {
	echo "<h2>We cannot handle this request right now. Please try again later.</h2>\n";
	diagnostics();
	/*
	 * Close out the web page
	 */
	echo "</body>\n";
	echo "</html>\n";
	write_log("INFO", MY_PID, SCRIPT_NAME . " ending in disgrace");
	exit(1);
}
/*
 * Get some convention info
 */
if ( isset($conv_id) ) {
	$conv_info_list = db_lookup_conv($virtcon_dbx, $conv_id);
	if ( $conv_info_list !== false ) {
		define('CONV_NAME', $conv_info_list['name_visible']);
	}
} else {
	$conv_info_list = db_match_request($virtcon_dbx, $conv_server);
	if ( $conv_info_list !== false ) {
		$conv_id = $conv_info_list['conv_id'];
		define('CONV_NAME', $conv_info_list['name_visible']);
	}
}
/*
 * At this point, if we do not have a $conv_id, then we really can't do anything else.
 */
if ( !isset($conv_id) ) {
	echo "<h2>We cannot handle this Convention. Please check the URL and contact support for your convention.</h2>\n";
	diagnostics();
	/*
	 * Close out the web page
	 */
	echo "</body>\n";
	echo "</html>\n";
	write_log("INFO", MY_PID, SCRIPT_NAME . " ending in disgrace");
	exit(1);
}
/*
 * If $error_flag is false, that means that we have the information we need,
 * so we just verify it and process the registration.
 */
if ( $error_flag === false ) {
	/*
     * Look for the submitted $conv_id, $one_time_code, and $otc_guid in the one_time_codes
     * table, if we find a matching ROW, then load the registration data into the
     * member_info table and send the user to the virtual con page with the GUID
     */
	if ( check_one_time_code($virtcon_dbx, "$conv_id", "$one_time_code") === false ) {
		/*
		 * The verification failed.
		 */
		write_log("OTC_WARN", MY_PID, "Could not find a match for {$conv_id}, {$one_time_code}");
		echo "</head>\n";
		echo "<body>\n";
		echo "<p>The ONE TIME CODE you submitted is not valid, please check it and re-enter below.</p>\n";
		echo "<p>If you continue to get this message, please contact support.</p>\n";
		$otc_guid = uuid_create(UUID_TYPE_RANDOM);
		member_info_form("$conv_id", CONV_NAME, "$conv_url", "$otc_guid", true);
	} else {
		/*
		 * Need to check to see if this person (email) already has a registration for this convention.
		 */
		$check_email_result = db_check_email($virtcon_dbx, "$conv_id", "$email");
		if ( $check_email_result === 0 ) {
			/*
			 * Load the registration information for the member.
			 */
			$timestamp = date('c');
			if ( add_member($virtcon_dbx, $otc_guid, $conv_id, $timestamp, $email, $badge_name, $first_name, $last_name) === false ) {
				/*
				 * Capture the DB error
				 */
				$db_error = mysqli_error($virtcon_dbx);
				/*
				 * Adding the member failed
				 */
				write_log("OTC_WARN", MY_PID, "Failed to add member {$otc_guid}, {$conv_id}, {$email}, {$badge_name}, {$first_name}, {$last_name}");
				write_log("DB_ERROR", MY_PID, "$db_error");
				echo "</head>\n";
				echo "<body>\n";
				echo "<p>We were not able to process your registration at this time. Please wait at least 10 minutes and try again.</p>\n";
				echo "<p>If you continue to get this message, please contact support.</p>\n";
			} else {
				/*
				 * Remove the OTC as it has just been used
				 */
				if ( remove_one_time_code($virtcon_dbx, $conv_id, $one_time_code, $otc_guid) === false ) {
					write_log("OTC_WARN", MY_PID, "Failed to remove OTC {$conv_id}, {$one_time_code}, {$otc_guid}");
				}
				/*
				 * HTML redirect to the main page with the GUID configured
				 */
				$web_server_name = $conv_info_list['virt_server'];
				echo "	<META HTTP-EQUIV='refresh' CONTENT='0; URL=https://{$web_server_name}/?auth={$otc_guid}'>\n";
				echo "</head>\n";
				echo "<body>\n";
				echo "<p>If you are seeing this, then your browser did not reload the correct page. Please click the link below to
    continue.</p>\n";
				echo "<a href='https://{$web_server_name}/?auth={$otc_guid}'>https://{$web_server_name}/?auth={$otc_guid}</a>\n";
			}
		} elseif ( $check_email_result === false ) {
			/*
			 * The check email DB query failed
			 */
			write_log("ERROR", MY_PID, "Failed to complete db_check_email query");
			// TODO User error message
		} else {
			/*
			 * We found an existing registration for this email address
			 */
			$otc_guid = $check_email_result[0]['guid'];
			write_log("INFO", MY_PID, "Found existing registration for {$email} with {$otc_guid}");
			/*
			 * Remove the OTC as it has just been used
			 */
			if ( remove_one_time_code($virtcon_dbx, $conv_id, $one_time_code, $otc_guid) === false ) {
				write_log("OTC_WARN", MY_PID, "Failed to remove OTC {$conv_id}, {$one_time_code}, {$otc_guid}");
			}
			/*
			 * HTML redirect to the main page with the GUID configured
			 */
			$web_server_name = $conv_info_list['virt_server'];
			echo "	<META HTTP-EQUIV='refresh' CONTENT='0; URL=https://{$web_server_name}/?auth={$otc_guid}'>\n";
			echo "</head>\n";
			echo "<body>\n";
			echo "<p>If you are seeing this, then your browser did not reload the correct page. Please click the link below to
    continue.</p>\n";
			echo "<a href='https://{$web_server_name}/?auth={$otc_guid}'>https://{$web_server_name}/</a>\n";
		}
	}
} else {
	/*
	 * The $error_flag was set to true, so we are missing onr or more input fields,
	 * so we need to figure out which and present the user with the correct form.
	 */
	if ( !isset($one_time_code) ) {
		/*
		 * We are missing the OTC, so we need to ask the user for everything
		 */
		/*
		 * Now capture all the user info, including the OTC
		 */
		$otc_guid = uuid_create(UUID_TYPE_RANDOM);
		member_info_form("$conv_id", CONV_NAME, "$conv_url", "$otc_guid", true);
	} elseif ( !isset($otc_guid) ) {
		/*
		 * Now we have the OTC but are missing the GUID, so the use must be using a pre-built link (QR code),
		 * so we need to ask the user for everything we are missing
		 */
		/*
		 * Verify the OTC
		 */
		if ( check_one_time_code($virtcon_dbx, "$conv_id", "$one_time_code") === false ) {
			/*
			 * OTC check failed, ask the user to resubmit it along with everything else
			 */
			echo "</head>\n";
			echo "<body>\n";
			echo "<p>The ONE TIME CODE you submitted is not valid, please check it and re-enter below.</p>\n";
			echo "<p>If you continue to get this message, please contact support.</p>\n";
			$otc_guid = uuid_create(UUID_TYPE_RANDOM);
			member_info_form("$conv_id", CONV_NAME, "$conv_url", "$otc_guid", true);
		} else {
			/*
			 * We have a valid OTC, so ask for the member info
			 */
			$otc_guid = uuid_create(UUID_TYPE_RANDOM);
			member_info_form("$conv_id", CONV_NAME, "$conv_url", "$otc_guid", "$one_time_code");
		}
	} elseif ( !isset($first_name) || !isset($last_name) || !isset($email) ) {
		/*
		 * We have the GUID, but we are missing one of the three required fields,
		 * resubmit that information.
		 */
		echo "</head>\n";
		echo "<body>\n";
		echo "<p>Please enter all of the REQUIRED information below.</p>\n";
		echo "<p>The following are all REQUIRED.</p>\n";
		echo "<ul><li>First Name</li><li>Last Name</li><li>Email Address</li></ul>\n";
		member_info_form("$conv_id", CONV_NAME, "$conv_url", "$otc_guid", "$one_time_code");
	}
}
if ( isset($virtcon_dbx) ) {
	if ( $virtcon_dbx !== false ) {
		mysqli_close($virtcon_dbx);
	}
}
diagnostics();
/*
 * Close out the web page
 */
echo "</body>\n";
echo "</html>\n";
write_log("INFO", MY_PID, SCRIPT_NAME . " ending");
exit(0);
