<?php
/**
 * Copyright 2024 Paul Kraus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

/*
 * Default main.php
 */
if ( isset($_REQUEST['page']) ) {
	switch ($_REQUEST['page']) {
		case "memberlist":
			log_page_access($virtcon_dbx, $conv_id, $member_info_list['guid'], "memberlist");
			member_list($conv_id);
			break;
		case "schedule":
			log_page_access($virtcon_dbx, $conv_id, $member_info_list['guid'], "schedule");
			echo "<iframe class='content' src='https://$convention_info_list[virt_server]/" . CONV_NAME . "/" . CONV_YEAR . "$convention_info_list[schedule_path]' title='{$convention_info_list['name_visible']}'></iframe>";
			break;
		case "film":
			log_page_access($virtcon_dbx, $conv_id, $member_info_list['guid'], "film");
			film_page($conv_id);
			break;
		case "usage":
			log_page_access($virtcon_dbx, $conv_id, $member_info_list['guid'], "usage");
			usage_reports($conv_id);
			break;
		case "email":
			log_page_access($virtcon_dbx, $conv_id, $member_info_list['guid'], "email");
			manage_email($conv_id);
			break;
		default:
			log_page_access($virtcon_dbx, $conv_id, $member_info_list['guid'], "home");
			convention_home($convention_info_list);
			break;
	}
} else {
	log_page_access($virtcon_dbx, $conv_id, $member_info_list['guid'], "home");
	convention_home($convention_info_list);
}
