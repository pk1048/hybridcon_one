<?php
/**
 * Copyright 2024 Paul Kraus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

/*
 * Albacon 2021 Menu
 */
echo "<div class='left_nav'>";
echo "<p>You are: {$_SESSION['name_first']} {$_SESSION['name_last']}";
echo "<br>{$_SESSION['email']}</p>";
if ( $_SESSION['name_badge'] != "" ) {
	echo "<p>Your badge name is: {$_SESSION['name_badge']}</p>";
}
echo "<h3>Go to:</h3>";
echo "<ul>";
echo "<li><a href='index.php'>Home</a></li>";
echo "<li><a href='index.php?page=schedule'>Schedule</a></li>";
echo "<li><a href='index.php?page=film'>Film / Anime Schedule</a></li>";
echo "<li><a href='index.php?page=memberlist'>Membership List</a></li>";
if ( $_SESSION['flag_admin'] == 1 ) {
	echo "<li><a href='index.php?page=email'>Send Email</a></li>";
}
echo "</ul>";
echo "</div>\n"; // Close the left_nav div
