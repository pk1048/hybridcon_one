#!/usr/local/bin/php
<?php
/**
 * Copyright 2024 Paul Kraus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

/*
 * Script to pull program info from Zambia for use by the virtcon konOpas
 */
$zambia_path = '/usr/local/www/albacon2021_zambia';
$virtcon_path = '/usr/local/www/albacon2021_virtual/dev';
require_once("{$virtcon_path}/virtcon.config");
require_once("{$virtcon_path}/konOpas_func.php");
require_once("{$virtcon_path}/include/log_functions.php");
$log_file = "{$log_dir}/konOpas.log";
$konopas_file = "{$virtcon_path}/htdocs/konOpasData.jsonp";
$mypid = getmypid();
$results = retrieveKonOpasData();
if ( $results["message_error"] ) {
	write_log("ERROR", MY_PID, "{$results['message_error']}");
	exit(1);
} else if ( $results["json"] ) {
	$resultsFile = fopen("{$konopas_file}", "wb");
	if ( $resultsFile === false ) {
		write_log("ERROR", MY_PID, "Can't open {$konopas_file} for writing.");
		exit(1);
	}
	if ( fwrite($resultsFile, $results["json"]) === false ) {
		write_log("ERROR", MY_PID, "Error writing to {$konopas_file}.");
		exit(1);
	}
	exit();
} else {
	write_log("ERROR", MY_PID, "retrieveKonOpasData() did not return expected result or error indicator.");
	exit(1);
}
?>
