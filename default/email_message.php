<?php
/**
 * Copyright 2024 Paul Kraus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

$email_subject = "Welcome, Online Login Information";
$email_message_text = "Welcome to {$conv_info_list['name_visible']}.\r\n\r\nHere is your link to access the virtual component of {$conv_info_list['name_visible']}.\r\n\r\nPlease note that this site is still being actively worked on, just like in-person conventions we are working right up the last minute (and maybe even after). So if something looks odd, or is broken, or changes, do not dismay.\r\n\r\nYour link to participate in the convention is:\r\n{$conv_auth_link}\r\n\r\nIf you have any problems, please either email us at {$conv_info_list['reg_email']} or {$conv_info_list['help_contact']} \r\n\r\n";

